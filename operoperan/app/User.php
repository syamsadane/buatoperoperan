<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected  $table = 'PLYR_MST';
    protected $primaryKey = 'PLYR_ID';

    protected $guarded = ['created_at', 'updated_at', 'remember_token'];

    public function getAuthPassword()
    {
        return $this->attributes['PLYR_PASSWORD'];
    }

//    public function getAuthIdentifier()
//    {
//        return $this->attributes['PLYR_MST_ID'];
//    }
//
//    public function getAuthIdentifierName()
//    {
//        return 'PLYR_MST_ID';
//    }
}
