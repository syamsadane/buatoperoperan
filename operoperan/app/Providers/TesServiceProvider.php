<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\TesService;

class TesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TesService::class, function () {
            return new TesService();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
