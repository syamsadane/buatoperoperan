<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use app\Services\ArenaService;

class ArenaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ArenaService::class, function () {
            return new ArenaService();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
