<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\MatchService;

class MatchServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(MatchService::class, function () {
            return new MatchService();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
