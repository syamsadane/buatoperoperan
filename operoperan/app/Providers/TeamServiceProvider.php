<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\TeamService;

class TeamServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TeamService::class, function () {
            return new TeamService();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
