<?php
/**
 * Created by PhpStorm.
 * User: Gerryger
 * Date: 9/16/16
 * Time: 4:51 PM
 */

namespace App\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CreateTeamRequest;
use App\Models\Team;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use App\Models\TeamPlayerDtl;
use App\Models\Player;


class TeamService
{
    private $dt;
    private $milliseconds;

    public function __construct()
    {
        $this->dt = new \DateTime();
        $this->milliseconds = round(microtime(true) * 1000);
    }

    public function getAllPlayer()
    {
        $players = Player::where('PLYR_FLAG_TEAM', '=', 0)
                            ->where('PLYR_FLAG_ACTIVE', '=', 0)
                            ->where('IS_ADMIN', 'N')
                            ->where('PLYR_EMAIL', '<>', Auth::user()->PLYR_EMAIL)
                            ->paginate(5);

        return $players;
    }

    public function postCreateTeam(CreateTeamRequest $request)
    {


        if($request->hasFile('teamLogo'))
        {
            //insert into TEAM_MST
            $team = new Team();

            $team->team_name = $request->input('teamName');
            $team->team_gender = $request->input('gender');
            $team->team_desc = $request->input('teamDescription');
            $team->team_city = $request->input('teamCity');
            $team->team_logo = $request->input('teamLogo');

            $imageName = 'img_team_'.$this->dt->format('Ymd') . $this->milliseconds . '.' . $request->file('teamLogo')->getClientOriginalExtension();
            $imageThumbName = 'thumb_img_team_'.$this->dt->format('Ymd').$this->milliseconds .'.' . $request->file('teamLogo')->getClientOriginalExtension();

            Image::make($request->file('teamLogo'))->fit(200, 200)->save(public_path('image/jpg/' . $imageName))->destroy();
            Image::make($request->file('teamLogo'))->fit(60, 60)->save(public_path('image/jpg/' . $imageThumbName))->destroy();

            $team->team_logo =  $imageName;  /**  base_path('public/image/jpg/' . $imageName);*/


            if ($team->save())
            {
                //update PLYR_FLAG_TEAM
                $player = Player::find(Auth::user()->PLYR_ID);
                $player->PLYR_FLAG_TEAM = 1;
                $player->save();

                $players = explode(',', $request->input('playerInput')[0]);
                Log::debug($players);

                foreach ($players as $player) {
                    //insert into TEAM_PLYR_DTL
                    $team->TeamPlayerDtl()->create([
                        'PLYR_ID' => $player,
                    ]);
                }


                $obj = ['status' => true, 'msg' => 'Success creating a team!'];

            }
            else
            {
                $obj = ['status' => false, 'msg' => 'FAILED creating a team!'];
            }
        }
        else
        {
            $obj = ['status' => false, 'msg' => 'Image doesn\'t exist!'];
        }

        return $obj;
    }

}