<?php
/**
 * Created by PhpStorm.
 * User: ggunawan
 * Date: 9/9/2016
 * Time: 12:51 PM
 */

namespace App\Services;


use App\Http\Controllers\Auth\AuthController;
use App\Http\Requests\CreatePlayerRequest;
use App\Http\Requests\UpdatePlayerSettingsRequest;
use App\Models\Player;
use App\Models\TeamPlayerDtl;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RegisterImageUploadRequest;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use App\Models\Team;
use App\Models\Image as ImageMst;
use Illuminate\Support\Facades\Hash;


class CustomAuthService extends AuthController
{
    protected $redirectTo = 'home';

    private $dt;
    private $milliseconds;

    public function __construct()
    {
        $this->dt = new \DateTime();
        $this->milliseconds = round(microtime(true) * 1000);
    }

    public function showLoginForm()
    {
        return parent::showLoginForm();
    }

    public function createPlayer(CreatePlayerRequest $request)
    {
        $players = new Player();

        $confirmation_code = str_random(30);

        $players->PLYR_FIRST_NAME = $request->input('firstName');
        $players->PLYR_LAST_NAME = $request->input('lastName');
        $players->PLYR_EMAIL = $request->input('registerEmail');
        $players->PLYR_GENDER = $request->input('gender');
        $players->PLYR_DOB = $request->input('birthDate');
        $players->PLYR_PASSWORD = bcrypt($request->input('registerPassword'));
        $players->PLYR_CONFIRMATION_CODE = $confirmation_code;

        if ($players->save()) {
            return $players;
        }

    }

    public function sendEmailConfirmation($playerEmail, $confirmationCode)
    {
        if (
            Mail::send('auth.emails.confirmationEmail', ['confirmationCode'=>$confirmationCode], function($m) use ($playerEmail) {
                $m->from('operoperan123@gmail.com', 'OPEROPERAN');
                $m->to($playerEmail);
                $m->subject('Verify your account');
            })
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function confirmAccount($confirmationCode)
    {
        if ($confirmationCode != null)
        {
            $player = Player::where('PLYR_CONFIRMATION_CODE', $confirmationCode)->first();

            if ($player != null)
            {
                $player->PLYR_CONFIRMED = 1;
                $player->PLYR_CONFIRMATION_CODE = null;

                if ($player->save())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public function showPlayerDetail()
    {
        $playerid = Auth::user()->PLYR_ID;

//        $team_plyr_dtl = TeamPlayerDtl::where('plyr_id','=',$playerid)->get(['TEAM_PLYR_DTL_ID']);

        $teamID = TeamPlayerDtl::where('PLYR_ID', '=', $playerid)->first();

        $teamplayer = \DB::table('TEAM_MST AS A')
            ->join('TEAM_PLYR_DTL AS B', 'A.TEAM_MST_ID', '=', 'B.TEAM_ID')
            ->join('PLYR_MST AS C', 'B.PLYR_ID', '=', 'C.PLYR_ID')
            ->select(\DB::raw('A.TEAM_MST_ID, A.TEAM_NAME, A.TEAM_LOGO, C.PLYR_ID, C.PLYR_FIRST_NAME, C.PLYR_LAST_NAME, C.PLYR_PROFILE_IMG'))
            ->where('A.TEAM_MST_ID', '=', $teamID->TEAM_ID)
            ->get();

        return view('player-detail', array('player' => Auth::user()),compact('teamplayer'));
    }


    public function postLogin(Request $request)
    {
        if (Auth::attempt(['PLYR_EMAIL' => $request->input('PLYR_EMAIL'), 'password' => $request->input('password'), 'PLYR_CONFIRMED' => 1])) {
            return redirect()->route($this->redirectTo)->with('success', 'You Have Logged In');
        } else {
            return redirect()->back()->with('error', 'Invalid credentials!');
        }
    }

    public function sendEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->passes()) {
            $player = Player::where('plyr_email', $request->input('email'))->first();
            $tempPass = str_random(5);

            if ($player != null) {
//                dd($player);
                $player->PLYR_PASSWORD = bcrypt($tempPass);

                if ($player->save()) {

                    if (
                    Mail::send('auth.emails.forgotpassword', ['newpass' => $tempPass], function ($m) use ($player) {
                        $m->from('operoperan123@gmail.com', 'TESTTT');
                        $m->to($player->PLYR_EMAIL);
                        $m->subject('TEST EMAIL');
                    })
                    ) {

//                        if (Auth::login($player)){
//                            return redirect()->route('home')->with('success', 'Success sending email!');
//                        } else {
//
//                        }
                        return redirect()->route('home')->with('success', 'Success sending email!');
//                        echo 'success <br/>'.'new password: '.$player->PLYR_PASSWORD;
                    } else {
                        return redirect()->route('home')->with('error', 'FAILED sending email!');
//                    echo 'error';
                    }
                }

            } else {
                return redirect()->back()->with('error', 'Email not found!');
//                echo 'not found';
            }
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function postInsertPersonalInfo(RegisterImageUploadRequest $request)
    {
        $imageName = 'img_player_' . $this->dt->format('Ymd') . $this->milliseconds . '.' . $request->file('image')->getClientOriginalExtension();
        $imageThumbName = 'thumb_img_player_' . $this->dt->format('Ymd') . $this->milliseconds . '.' . $request->file('image')->getClientOriginalExtension();
        $city = $request->input('city');
        $favouriteNumber = $request->input('favouriteNumber');

        $player = Player::find(Session::get('PLYR_ID'));


        if ($player != null) {
            Image::make($request->file('image'))->fit(200, 200)->save(public_path('/image/jpg/' . $imageName))->destroy();
            Image::make($request->file('image'))->fit(60, 60)->save(public_path('/image/jpg/' . $imageThumbName))->destroy();

            $player->PLYR_PROFILE_IMG = $imageName;
            /**  base_path('public/image/jpg/' . $imageName); */
            $player->PLYR_CITY = $city;
            $player->PLYR_FAVOURITE_NO = $favouriteNumber;


            if ($player->save()) {
                return true;
            } else {
                return false;
            }
        }
    }


    public function updatePlayerSettings(UpdatePlayerSettingsRequest $request)
    {
        $playerid = Auth::user()->PLYR_ID;

        $player = Player::where('PLYR_ID', '=', $playerid)->find($playerid);

        $player->PLYR_CITY = $request->input('city');
        $player->PLYR_PHONE = $request->input('phoneNumber');
        $player->PLYR_FAVOURITE_NO = $request->input('favouriteNo');


        if ($request->hasFile('image')) {
            $player->PLYR_PROFILE_IMG = $request->input('image');
            $imageName = 'img_player_' . $this->dt->format('Ymd') . $this->milliseconds . '.' . $request->file('image')->getClientOriginalExtension();
            $imageThumbName = 'thumb_img_player_' . $this->dt->format('Ymd') . $this->milliseconds . '.' . $request->file('image')->getClientOriginalExtension();

            Image::make($request->file('image'))->fit(200, 200)->save(public_path('/image/jpg/' . $imageName))->destroy();
            Image::make($request->file('image'))->fit(60, 60)->save(public_path('/image/jpg/' . $imageThumbName))->destroy();

            $player->PLYR_PROFILE_IMG = $imageName;

            if ($player->update()) {
                return true;
            } else {
                return false;
            }
        }
        $player->PLYR_CITY = $request->input('city');
        $player->PLYR_PHONE = $request->input('phoneNumber');
        $player->PLYR_FAVOURITE_NO = $request->input('favouriteNo');

        if ($player->update()) {
            return true;
        } else {
            return false;
        }

    }

    public function updatePlayerPassword(Request $request)
    {
        $playerid = Auth::user()->PLYR_ID;
        $player = Player::where('PLYR_ID', '=', $playerid)->find($playerid);


        $this->validate($request, [
            'oldPassword' => 'required|min:4',
            'newPassword' => 'required|different:oldPassword|confirmed',
        ]);

//        tolong dilanjutkan sodara gerry.. untuk indonesia yg lbh cerah

        if($player->update()){
            return true;
        }else{
            return false;
        }


    }


}