<?php
/**
 * Created by PhpStorm.
 * User: Gerryger
 * Date: 9/16/16
 * Time: 4:40 PM
 */

namespace app\Services;

use Illuminate\Http\Request;
use App\Http\Requests\CreateMatchChallengeRequest;
use App\Models\Match;
use Illuminate\Support\Facades\Auth;
use App\Models\Player;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\TeamPlayerDtl;

class MatchService
{
    public function __construct()
    {
        
    }

    public function getAvailableMatches()
    {
        $select = "  	A.MATCH_MST_ID, " .
		                    " A.MATCH_DATE, " .
                            " A.MATCH_TIME, " .
                            " A.MATCH_LOC, " .
                            " case A.MATCH_TYPE " .
        	                    " when 'C' then 'Challenge' " .
                                " else 'Fun' " .
                            " end as MATCH_TYPE, " .
                            " A.MATCH_FEE_TYPE, " .
                            " C.TEAM_MST_ID, " .
                            " C.TEAM_LOGO, " .
                            " D.ARENA_ID, " .
                            " D.ARENA_NAME, " .
                            " D.ARENA_CITY ";

        $matches = DB::table('MATCH_MST AS A')
                        ->join('TEAM_PLYR_DTL AS B', 'A.MATCH_CREATED_BY', '=', 'B.PLYR_ID')
                        ->join('TEAM_MST AS C', 'C.TEAM_MST_ID', '=', 'B.TEAM_ID')
                        ->join('ARENA_MST AS D', 'D.ARENA_ID', '=', 'A.MATCH_LOC')
                        ->select(DB::raw($select))
                        ->where('A.MATCH_FLAG', '=', 0)
                        ->orderBy('A.created_at', 'desc')
                        ->take(5)
                        ->get();

        return $matches;
    }

    public function getTeamsByPlayer($plyrID)
    {
        $teamID = TeamPlayerDtl::where('PLYR_ID', '=', $plyrID)->first();

        $players = DB::table('TEAM_MST AS A')
                        ->join('TEAM_PLYR_DTL AS B', 'A.TEAM_MST_ID', '=', 'B.TEAM_ID')
                        ->join('PLYR_MST AS C', 'B.PLYR_ID', '=', 'C.PLYR_ID')
                        ->select(DB::raw('A.TEAM_MST_ID, A.TEAM_NAME, A.TEAM_LOGO, C.PLYR_ID, C.PLYR_FIRST_NAME, C.PLYR_LAST_NAME, C.PLYR_PROFILE_IMG'))
                        ->where('A.TEAM_MST_ID', '=', $teamID->TEAM_ID)
                        ->get();

        return $players;
    }

    public function postCreateMatchChallenge(CreateMatchChallengeRequest $request)
    {
        $matchChallenge = new Match();
        $matchChallenge->MATCH_DATE = $request->input('date');
        $matchChallenge->MATCH_TIME = $request->input('time');
        $matchChallenge->MATCH_LOC = $request->input('futsalArena');
        $matchChallenge->MATCH_FEE_TYPE = $request->input('payment-type');
        $matchChallenge->MATCH_CREATED_BY = Auth::user()->PLYR_ID;
        $matchChallenge->MATCH_TYPE = 'C';
        $matchChallenge->MATCH_FLAG = 0;

        if ($matchChallenge->save())
        {
            $matchChallenge->MatchTeamDtl()->create([
                'TEAM_ID' => $request->input('teamID'),
            ]);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function postSendInvitation(Request $request)
    {
        $email = $request->input('email');
        $player = Player::find($request->input('playerID'));
        $playerName = $player->PLYR_FIRST_NAME.' '.$player->PLYR_LAST_NAME;

        $message = ['email.required' => 'Email required!', 'email.email' => 'Invalid email format!'];

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ], $message);

        if ($validator->fails())
        {
            //email format
            return 1;
        }
        else
        {
            if (count(Player::where('PLYR_EMAIL', '=', $email)->first()) > 0) {
                //email exist
                return -1;
            } else {
                if (Mail::send('auth.emails.matchInvitation', ['playerName' => $playerName], function ($m) use ($email) {
                    $m->from('operoperan123@gmail.com', 'TESTTT');
                    $m->to($email);
                    $m->subject('TEST INVITATION EMAIL');
                })
                ) {
                    //sent successfully
                    return 0;
                } else {
                    //failed sending email
                    return -2;
                }
            }
        }
    }
}