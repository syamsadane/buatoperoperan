<?php
/**
 * Created by PhpStorm.
 * User: Gerryger
 * Date: 10/8/16
 * Time: 9:10 AM
 */

namespace app\Services;

use App\Models\Arena;

class ArenaService
{
    public function __construct()
    {

    }

    public function getAllArena()
    {
        $arena = Arena::all();

        return $arena;
    }
}