<?php
/**
 * Created by PhpStorm.
 * User: Gerryger
 * Date: 10/3/16
 * Time: 9:47 PM
 */

namespace app\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyCreateTeam
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->PLYR_FLAG_TEAM != 1)
        {
            return redirect()->route('home');
        }

        return $next($request);
    }
}