<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use app\Services\ArenaService;
use App\Models\Arena;

class ArenaController extends Controller
{
    private $arenaService;

    public function __construct(ArenaService $arenaService)
    {
        $this->arenaService = $arenaService;
    }
}
