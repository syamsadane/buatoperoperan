<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TeamService;
use App\Http\Requests\CreateTeamRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    private $teamService;

    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    public function showCreateTeamForm()
    {
        if (Auth::user()->PLYR_FLAG_TEAM > 0)
        {
            return redirect()->route('home')->with('error', 'You\'re already on a team!');
        }
        else
        {
            $players = $this->teamService->getAllPlayer();
            return view('create-team', ['players' => $players]);
        }
    }

    public function postCreateForm(CreateTeamRequest $request)
    {
        $result = $this->teamService->postCreateTeam($request);

        if ($result['status'])
        {
            return redirect()->route('team-detail')
                ->with('success',$result['msg']);
        }
        else
        {
            return redirect()->back()->with('error', $result['msg']);
        }
    }
}
