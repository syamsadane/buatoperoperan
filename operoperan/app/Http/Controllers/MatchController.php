<?php

namespace App\Http\Controllers;

use App\Services\ArenaService;
use Illuminate\Http\Request;
use App\Services\MatchService;
use App\Http\Requests\CreateMatchChallengeRequest;
use Illuminate\Support\Facades\Auth;

class MatchController extends Controller
{
    private $matchService;
    private $arenaService;

    public function __construct(MatchService $matchService, ArenaService $arenaService)
    {
        $this->matchService = $matchService;
        $this->arenaService = $arenaService;
    }

    public function showCreateChallengeMatchForm()
    {
        if (Auth::user()->PLYR_FLAG_TEAM == 0)
        {
            return redirect()->route('home')->with('error', 'You\'re not on a team. Please join team first!');
        }
        else {
            $players = $this->matchService->getTeamsByPlayer(Auth::user()->PLYR_ID);
            $arenas = $this->arenaService->getAllArena();

            $teamLogo = $players[0]->TEAM_LOGO;
            $teamName = $players[0]->TEAM_NAME;
            $teamID = $players[0]->TEAM_MST_ID;

            return view('match.create-match-challenge', [
                    'players' => $players,
                    'teamLogo' => $teamLogo,
                    'teamName' => $teamName,
                    'teamID' => $teamID,
                    'arenas' => $arenas,
                ]
            );
        }
    }

    public function postCreateMatchChallenge(CreateMatchChallengeRequest $request)
    {
        if ($this->matchService->postCreateMatchChallenge($request))
        {
            return redirect()->route('home')->with('success', 'Match created!');
        }
        else
        {
            return redirect()->route('home')->with('error', 'Match creation FAILED!');
        }
    }

    public function postSendInvitation(Request $request)
    {

        if ($this->matchService->postSendInvitation($request) == 0)
        {
            return response()->json(['status' => true, 'msg' => 'Success sending invitation email!']);
        }
        else if ($this->matchService->postSendInvitation($request) == 1)
        {
            return response()->json(['status' => false, 'msg' => 'Email is required and must be in email format!']);
        }
        else if ($this->matchService->postSendInvitation($request) == -1)
        {
            return response()->json(['status' => false, 'msg' => 'This email is already registered!']);
        }
        else
        {
            return response()->json(['status' => false, 'msg' => 'Error sending email!']);
        }
    }

    public function showCreateFunMatchForm()
    {
        return view('match.create-match-fun');
    }



}
