<?php

namespace App\Http\Controllers;

use App\Services\TesService;
use Illuminate\Http\Request;
use App\Http\Requests;

class TesController extends Controller
{
    private $tesService;

    public function __construct(TesService $tesService)
    {
        $this->tesService = $tesService;
    }

    public function printTest()
    {
        return $this->tesService->test();
    }
}
