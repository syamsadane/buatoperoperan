<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterImageUploadRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdatePlayerSettingsRequest;
use App\Services\CustomAuthService;
use Illuminate\Http\Request;
use App\Http\Requests\CreatePlayerRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CustomAuthController extends Controller
{
    private $customAuthService;

    public function __construct(CustomAuthService $customAuthService)
    {
        $this->customAuthService = $customAuthService;
    }

    public function showLoginForm()
    {
        return $this->customAuthService->showLoginForm();
    }



    public function createPlayer(CreatePlayerRequest $request)
    {
        $players = $this->customAuthService->createPlayer($request);

        if ($players != null)
        {
            if ($this->customAuthService->sendEmailConfirmation($players->PLYR_EMAIL, $players->PLYR_CONFIRMATION_CODE))
            {
                Session::flash('success', 'Confirmation email sent!');
                return redirect()->route('auth.insertPersonalInfo')->with('PLYR_ID', $players->PLYR_ID);
            }
            else
            {
                Session::flash('error', 'Unable to send confirmation email!');
                return redirect()->route('auth.insertPersonalInfo')->with('PLYR_ID', $players->PLYR_ID);
            }
        }
        else
        {
            return redirect()->route('home')->with('error','ERROR!');
        }
    }


    public function showPlayerDetail()
    {
         return $this->customAuthService->showPlayerDetail();
    }



    public function postLogin(Request $request)
    {
        return $this->customAuthService->postLogin($request);
    }

    public function postLogout()
    {
        return $this->customAuthService->getLogout();
    }

    public function sendEmail(Request $request)
    {
        return $this->customAuthService->sendEmail($request);
    }

    public function insertPersonalInfo()
    {
        return view('auth.register-upload-image', ['plyr_id' => Session::get('PLYR_ID')]);
    }

    public function postInsertPersonalInfo(RegisterImageUploadRequest $request)
    {
        if($this->customAuthService->postInsertPersonalInfo($request))
        {
            return redirect()->route('auth.login')->with('success','Successfully Registered An Account, please confirm your account');
        }
        else
        {
            return redirect()->back()->with('error', 'Error on adding player personal information!');
        }
    }

    public function showPlayerSettings()
    {
        return view('auth.setting');
    }

    public function updatePlayerSettings(UpdatePlayerSettingsRequest $request)
    {
        if($this->customAuthService->updatePlayerSettings($request))
        {
            return redirect()->route('player.detail')->with('success','Successfully Updated Your Profile');
        }else
        {
            return redirect()->back()->with('error','Error on Updating Your Profile');
        }
    }

    public function updatePlayerPassword(Request $request)
    {
        if($this->customAuthService->updatePlayerPassword($request))
        {
            return redirect()->route('player.detail')->with('success','Successfully Updated Your Password');
        }else
        {
            return redirect()->back()->with('error','Error on Updating Your Password');
        }
    }

    public function confirmAccount($confirmationCode)
    {
        if ($this->customAuthService->confirmAccount($confirmationCode))
        {
            return redirect()->route('auth.login')->with('success', 'Account confirmed, please login!');
        }
        else
        {
            return redirect()->route('auth.login')->with('error', 'Failed to confirm account!');
        }
    }
}
