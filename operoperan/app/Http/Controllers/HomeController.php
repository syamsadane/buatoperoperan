<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\MatchService;

class HomeController extends Controller
{
    private $matchService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = $this->matchService->getAvailableMatches();
        return view('home', ['matches' => $matches]);
    }
}
