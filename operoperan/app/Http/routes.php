<?php

use Illuminate\Support\Facades\Auth;
use App\Models\Player;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/reset-default-pass', function () {
    $plyr = Player::where('PLYR_EMAIL', 'gabrielcaesario@gmail.com')->first();
    $plyr->PLYR_PASSWORD = bcrypt('asdasd');
    return $plyr->save() ? "success" : "FAIL";
});

Route::get('/',[
    'uses' => 'HomeController@index',
    'as' => 'home',
]);

Route::get('/operoperan-login-register', [
    'uses' => 'CustomAuthController@showLoginForm',
    'as' => 'auth.login',
    'middleware' => 'guest'
]);

Route::get('/operoperan-insert-personal-info', [
    'uses' => 'CustomAuthController@insertPersonalInfo',
    'as' => 'auth.insertPersonalInfo',
]);

Route::get('/operoperan-verify/{confirmationCode}', [
    'uses' => 'CustomAuthController@confirmAccount',
    'as' => 'auth.confirm',
    'middleware' => 'guest',
]);

Route::post('/operoperan-insert-personal-info', [
    'uses' => 'CustomAuthController@postInsertPersonalInfo',
    'as' => 'auth.insertPersonalInfo',
]);

Route::get('/operoperan-logout', [
    'uses' => 'CustomAuthController@postLogout',
    'as' => 'auth.logout',
    'middleware' => 'auth'
]);

Route::post('/operoperan-login-register', [
    'uses' => 'CustomAuthController@postLogin',
    'as' => 'auth.login',
   'middleware' => 'guest'
]);

Route::post('/operoperan-register', [
    'uses' => 'CustomAuthController@createPlayer',
    'as' => 'auth.register',
    'middleware' => 'guest',
]);

Route::post('/operoperan-reset-password', [
    'uses' => 'CustomAuthController@sendEmail',
    'as' => 'auth.resetPassword',
    'middleware' => 'guest',
]);


Route::get('/operoperan-player-detail',[
   'uses' =>'CustomAuthController@showPlayerDetail',
    'as' => 'player.detail',
    'middleware' => 'auth',
]);

Route::get('/operoperan-create-team', [
    'uses' => 'TeamController@showCreateTeamForm',
    'as' => 'team.create',
    'middleware' => 'auth',
]);

Route::post('/operoper-create-team', [
    'uses'=> 'TeamController@postCreateForm',
    'as' => 'team.post',
    'middleware' => 'auth',
]);

Route::get('/operoperan-match-challenge-create', [
    'uses' => 'MatchController@showCreateChallengeMatchForm',
    'as' => 'match.challenge.create',
    'middleware' => 'auth',
]);

Route::post('/operoperan-match-challenge-create', [
    'uses' => 'MatchController@postCreateMatchChallenge',
    'as' => 'match.challenge.create',
    'middleware' => 'auth',
]);

Route::post('/operoperan-sendInvitation', [
    'uses' => 'MatchController@postSendInvitation',
    'as' => 'match.sendIvitation',
    'middleware' => 'auth',
]);

Route::get('/operoperan-match-fun-create', [
   'uses' => 'MatchController@showCreateFunMatchForm',
    'as' => 'match.fun.create',
    'middleware' => 'auth',
]);

Route::get('/operoperan-player-settings',[
    'uses' => 'CustomAuthController@showPlayerSettings',
    'as' => 'auth.settings',
    'middleware'=>'auth',
]);

Route::POST('/operoperan-player-settings',[
    'uses' => 'CustomAuthController@updatePlayerSettings',
    'as' => 'auth.post.settings',
    'middleware'=>'auth',
]);

Route::POST('/operoperan-player-settings-password',[
    'uses' => 'CustomAuthController@updatePlayerPassword',
    'as' => 'auth.update.password',
    'middleware'=>'auth',
]);

Route::GET('/operoperan-search',function(){
    return view('search');
})->name('search');

Route::GET('/operoperan-arena',function(){
   return view('arena');
})->name('arena');

Route::get('/operoperan-team-detail' , function(){
    return view('team-detail');
})->name('team-detail');


//test controller to test service layer
Route::get('/tes', 'TesController@printTest');


//Route::get('/register', function () {
//    $player = new Player();
//    $player->PLYR_FIRST_NAME = 'gerry';
//    $player->PLYR_LAST_NAME = 'ganteng';
//    $player->PLYR_EMAIL = 'gerry@ganteng.com';
//    $player->PLYR_PASSWORD = bcrypt('asdasd');
//
//    if($player->save())
//    {
//        echo 'INSERT SUCCESS!';
//    }
//});

//Route::get('/login', 'Auth\AuthController@postLogin');

