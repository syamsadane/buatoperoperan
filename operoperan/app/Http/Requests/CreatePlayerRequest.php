<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Player;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class CreatePlayerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|min:3|max:255',
            'lastName' => 'required|min:3|max:255',
            'gender' => 'required',
            'registerEmail' => 'required|email|unique:PLYR_MST,PLYR_EMAIL',
            'registerPassword' => 'required',
            'confirmPassword' => 'required|same:registerPassword',
            'birthDate' => 'required',
        ];
    }

//    public function getRedirectUrl()
//    {
//        return URL::route('auth.login', ['id'=>'asdasd','#register']);
//    }

}
