<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterImageUploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpg,jpeg,png',
            'favouriteNumber' => 'required|numeric',
            'city' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'image.mimes' => 'The image format must be in jpg, jpeg or png!',
            'favouriteNumber.numeric' => 'Favourite number must be numeric!',
        ];
    }
}
