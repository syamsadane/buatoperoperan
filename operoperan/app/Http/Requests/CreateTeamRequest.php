<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateTeamRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'teamName' => 'required|unique:team_mst,team_name',
            'teamLogo' => 'required|mimes:jpeg,jpg,png,bmp',
            'teamDescription' =>'required|min:4',
            'teamCity' =>'required',
            'gender' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'teamName.required' => 'Team name required!',
            'teamName.unique' => 'Team name must be unique!',
            'teamLogo.required' => 'Team logo required',
            'teamLogo.mimes' => 'Only jpeg,jpg,png,bmp format allowed!',
            'teamDescription.required' => 'Team description required!',
            'teamDescription.min' => 'Team description must be equal or more than 4 characters!',
            'teamCity.required' => 'Team city required!',
            'gender' => 'Gender required!',
        ];
    }
}
