<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateMatchChallengeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'futsalArena' => 'required',
            'city' => 'required',
            'address' => 'required',
            'date' => 'required',
            'time' => 'required',
            'payment-type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'futsalArena.required' => 'Futsal arena required!',
            'payment-type.required' => 'Payment type required!',
            'city.required' => 'City required!',
            'address.required' => 'Address required!',
            'date.required' => 'Date required!',
            'time.required' => 'Time required!',
        ];
    }
}
