<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arena extends Model
{
    protected $table='arena_mst';
    protected $guarded = ['ARENA_ID', 'created_at', 'updated_at'];

    public function Match()
    {
        return $this->hasOne(Match::class);
    }

    public function ArenaDtl()
    {
        return $this->hasMany(ArenaDtl::class);
    }
}
