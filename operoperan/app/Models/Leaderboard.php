<?php

namespace App\Models;

use App\Team;
use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    protected $table='leaderboard_mst';


    public function Team()
    {
        return $this->belongsTo(Team::class);
    }
}
