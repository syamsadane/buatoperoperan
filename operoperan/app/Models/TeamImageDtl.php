<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;

class TeamImageDtl extends Model
{
    protected $table='team_image_dtl';

    public function Image()
    {
        return $this->belongsTo(Image::class);
    }

    public function Team()
    {
        return $this->belongsTo(Team::class);
    }
}
