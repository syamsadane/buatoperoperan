<?php

namespace App\Models;

use App\Models\Leaderboard;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team_mst';

    public function TeamPlayerDtl()
    {
        return $this->hasMany(TeamPlayerDtl::class);
    }

    public function TeamAchievementDtl()
    {
        return $this->hasMany(TeamAchievementDtl::class);
    }

    public function MatchTeamDtl()
    {
        return $this->hasOne(MatchTeamDtl::class);
    }

    public function TeamImageDtl()
    {
        return $this->hasMany(TeamImageDtl::class);
    }

    public function Leaderboard()
    {
        return $this->hasOne(Leaderboard::class);
    }



}
