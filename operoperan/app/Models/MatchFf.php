<?php

namespace App\Models;

use App\MatchFfPlayerDtl;
use Illuminate\Database\Eloquent\Model;

class MatchFf extends Model
{
    protected $table ='match_ff_mst';

    public function MatchFfPlayerDtl()
    {
        return $this->hasMany(MatchFfPlayerDtl::class);
    }
}
