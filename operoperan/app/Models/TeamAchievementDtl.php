<?php

namespace App\Models;

use App\Models\Achievement;
use Illuminate\Database\Eloquent\Model;

class TeamAchievementDtl extends Model
{
    protected $table='team_achievement_dtl';


    public function Team()
    {
        return $this->belongsTo(Team::class);
    }

    public function Achievement()
    {
        return $this->belongsTo(Achievement::class);
    }
}
