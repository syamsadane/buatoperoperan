<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArenaDtl extends Model
{
    protected $table='arena_dtl';
    protected $guarded = ['ARENA_DTL_ID', 'created_at', 'updated_at'];

    public function Arena()
    {
        return $this->belongsTo(Arena::class);
    }

    public function ArenaPriceDtl()
    {
        return $this->hasMany(ArenaPriceDtl::class);
    }
}
