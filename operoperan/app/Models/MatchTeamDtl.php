<?php

namespace App\Models;

use App\Models\Match;
use Illuminate\Database\Eloquent\Model;

class MatchTeamDtl extends Model
{
    protected  $table='match_team_dtl';
    protected $guarded = ['MATCH_TEAM_DTL_ID', 'created_at', 'updated_at'];

    public function Match()
    {
        return $this->belongsTo(Match::class);
    }

    public function Team()
    {
        return $this->belongsTo(Team::class);
    }

}
