<?php

namespace App\Models;

use App\MatchFfPlayerDtl;
use App\Models\TeamPlayerDtl;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Player extends Authenticatable
{
    protected  $table = 'PLYR_MST';
    protected $primaryKey = 'PLYR_ID';

    protected $guarded = ['PLYR_ID', 'created_at', 'updated_at', 'remember_token'];


    public function TeamPlayerDtl()
    {
        return $this->hasMany(TeamPlayerDtl::class);
    }

    public function MatchFfPlayerDtl()
    {
        return $this->hasMany(MatchFfPlayerDtl::Class);
    }

    public function ImageMst()
    {
        return $this->hasOne(Image::class);
    }

    public function getAuthPassword()
    {
        return $this->attributes['PLYR_PASSWORD'];
    }

    public function getKeyName()
    {
        return 'PLYR_ID';
    }

    public function getKey()
    {
        return $this->attributes['PLYR_ID'];
    }
}
