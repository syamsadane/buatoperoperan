<?php

namespace App;

use App\Models\MatchFf;
use App\Models\Player;
use Illuminate\Database\Eloquent\Model;

class MatchFfPlayerDtl extends Model
{
    protected $table='match_ff_plyr_dtl';


    public function MatchFf()
    {
        return $this->belongsTo(MatchFf::class);
    }

    public function Player()
    {
        return $this->belongsTo(Player::class);
    }


}
