<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArenaPriceDtl extends Model
{
    protected $table='arena_price_dtl';
    protected $guarded = ['ARENA_PRICE_DTL_ID', 'created_at', 'updated_at'];


    public function ArenaDtl()
    {
        return $this->belongsTo(ArenaDtl::class);
    }
}
