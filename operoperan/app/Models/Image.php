<?php

namespace App\Models;

use App\TeamImageDtl;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table='image_mst';
    protected $guarded = ['IMAGE_MST_ID', 'created_at', 'updated_at'];


    public function TeamImageDtl()
    {
        return $this->hasMany(TeamImageDtl::class);
    }

    public function Player()
    {
        return $this->belongsTo(Player::class);
    }
}
