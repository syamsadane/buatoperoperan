<?php

namespace App\Models;

use App\Models\TeamAchievementDtl;
use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $table='achievement_mst';
    protected $guarded = ['ACHIEVEMENT_MST_ID', 'created_at', 'updated_at'];


    public function TeamAchievementDtl()
    {
        return $this->hasMany(TeamAchievementDtl::class);
    }
}
