<?php

namespace App\Models;

use App\Models\Player;
use Illuminate\Database\Eloquent\Model;

class TeamPlayerDtl extends Model
{
    protected $table='team_plyr_dtl';
    protected $fillable = ['TEAM_ID', 'PLYR_ID'];

    public function Player()
    {
        return $this->belongsTo(Player::class);
    }

    public function Team()
    {
        return $this->belongsTo(Team::class);
    }
}
