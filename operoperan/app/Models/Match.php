<?php

namespace App\Models;

use App\Models\MatchTeamDtl;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table ='match_mst';


    public function Arena()
    {
        return $this->belongsTo(Arena::class);

    }

    public function MatchTeamDtl()
    {
        return $this->hasMany(MatchTeamDtl::class);
    }

}
