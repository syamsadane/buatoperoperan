-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2016 at 05:04 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `operoperan`
--

-- --------------------------------------------------------

--
-- Table structure for table `ACHIEVEMENT_MST`
--

CREATE TABLE IF NOT EXISTS `ACHIEVEMENT_MST` (
  `ACHIEVEMENT_MST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ACHIEVEMENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACHIEVEMENT_DESC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACHIEVEMENT_LOGO` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ACHIEVEMENT_MST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ARENA_DTL`
--

CREATE TABLE IF NOT EXISTS `ARENA_DTL` (
  `ARENA_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ARENA_ID` int(10) unsigned NOT NULL,
  `ARENA_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_GRASS_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_JUMLAH` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ARENA_DTL_ID`),
  KEY `arena_dtl_arena_id_foreign` (`ARENA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ARENA_MST`
--

CREATE TABLE IF NOT EXISTS `ARENA_MST` (
  `ARENA_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ARENA_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_CITY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_PHONE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_DESC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_FACILITY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_ALTITUDE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_LONGITUDE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ARENA_ID`),
  KEY `arena_mst_arena_name_index` (`ARENA_NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ARENA_MST`
--

INSERT INTO `ARENA_MST` (`ARENA_ID`, `ARENA_NAME`, `ARENA_CITY`, `ARENA_PHONE`, `ARENA_DESC`, `ARENA_FACILITY`, `ARENA_ALTITUDE`, `ARENA_LONGITUDE`, `created_at`, `updated_at`) VALUES
(1, 'de_dust2', 'Jakarta', '', '', '', '', '', '2016-10-07 17:00:00', '2016-10-07 17:00:00'),
(2, 'cs_italy', 'Italy', '', '', '', '', '', '2016-10-07 17:00:00', '2016-10-07 17:00:00'),
(3, 'cs_inferno', '', '', '', '', '', '', '2016-10-07 17:00:00', '2016-10-07 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ARENA_PRICE_DTL`
--

CREATE TABLE IF NOT EXISTS `ARENA_PRICE_DTL` (
  `ARENA_PRICE_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ARENA_DTL_ID` int(10) unsigned NOT NULL,
  `ARENA_HARI` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_JAM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ARENA_HARGA` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ARENA_PRICE_DTL_ID`),
  KEY `arena_price_dtl_arena_dtl_id_foreign` (`ARENA_DTL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `IMAGE_MST`
--

CREATE TABLE IF NOT EXISTS `IMAGE_MST` (
  `IMAGE_MST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IMAGE_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE_PATH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IMAGE_MST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `LEADERBOARD_MST`
--

CREATE TABLE IF NOT EXISTS `LEADERBOARD_MST` (
  `LEADERBOARD_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEAM_ID` int(10) unsigned NOT NULL,
  `LEADERBOARD_PLAY` int(11) NOT NULL,
  `LEADERBOARD_WIN` int(11) NOT NULL,
  `LEADERBOARD_DRAW` int(11) NOT NULL,
  `LEADERBOARD_LOSE` int(11) NOT NULL,
  `LEADERBOARD_GF` int(11) NOT NULL,
  `LEADERBOARD_GA` int(11) NOT NULL,
  `LEADERBOARD_GD` int(11) NOT NULL,
  `LEADERBOARD_PTS` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`LEADERBOARD_ID`),
  KEY `leaderboard_mst_team_id_foreign` (`TEAM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MATCH_FF_MST`
--

CREATE TABLE IF NOT EXISTS `MATCH_FF_MST` (
  `MATCH_FF_MST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MATCH_FF_DATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_FF_TIME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_FF_LOC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_FF_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_FF_FEE_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_FF_CREATED_BY` int(10) unsigned NOT NULL,
  `MATCH_FF_FLAG` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`MATCH_FF_MST_ID`),
  KEY `match_ff_mst_match_ff_created_by_foreign` (`MATCH_FF_CREATED_BY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MATCH_FF_PLYR_DTL`
--

CREATE TABLE IF NOT EXISTS `MATCH_FF_PLYR_DTL` (
  `MATCH_FF_PLYR_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MATCH_FF_ID` int(10) unsigned NOT NULL,
  `PLYR_ID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`MATCH_FF_PLYR_DTL_ID`),
  KEY `match_ff_plyr_dtl_match_ff_id_foreign` (`MATCH_FF_ID`),
  KEY `match_ff_plyr_dtl_plyr_id_foreign` (`PLYR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MATCH_MST`
--

CREATE TABLE IF NOT EXISTS `MATCH_MST` (
  `MATCH_MST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MATCH_DATE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_TIME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_LOC` int(10) unsigned NOT NULL,
  `MATCH_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_FEE_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MATCH_CREATED_BY` int(10) unsigned NOT NULL,
  `MATCH_FLAG` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`MATCH_MST_ID`),
  KEY `match_mst_match_loc_foreign` (`MATCH_LOC`),
  KEY `match_mst_match_created_by_foreign` (`MATCH_CREATED_BY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `MATCH_MST`
--

INSERT INTO `MATCH_MST` (`MATCH_MST_ID`, `MATCH_DATE`, `MATCH_TIME`, `MATCH_LOC`, `MATCH_TYPE`, `MATCH_FEE_TYPE`, `MATCH_CREATED_BY`, `MATCH_FLAG`, `created_at`, `updated_at`) VALUES
(1, '10/08/2016', '07:00 AM', 1, 'C', '50:50', 3, 0, '2016-10-07 20:10:02', '2016-10-07 20:10:02');

-- --------------------------------------------------------

--
-- Table structure for table `MATCH_TEAM_DTL`
--

CREATE TABLE IF NOT EXISTS `MATCH_TEAM_DTL` (
  `MATCH_TEAM_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MATCH_ID` int(10) unsigned NOT NULL,
  `TEAM_ID` int(10) unsigned NOT NULL,
  `SCORE` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`MATCH_TEAM_DTL_ID`),
  KEY `match_team_dtl_match_id_foreign` (`MATCH_ID`),
  KEY `match_team_dtl_team_id_foreign` (`TEAM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `MATCH_TEAM_DTL`
--

INSERT INTO `MATCH_TEAM_DTL` (`MATCH_TEAM_DTL_ID`, `MATCH_ID`, `TEAM_ID`, `SCORE`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 0, '2016-10-07 20:10:02', '2016-10-07 20:10:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2016_09_01_162640_create_image_mst_table', 1),
('2016_09_01_162641_create_plyr_mst_table', 1),
('2016_09_01_162642_create_arena_mst_table', 1),
('2016_09_01_162643_create_team_mst_table', 1),
('2016_09_01_162645_create_match_mst_table', 1),
('2016_09_01_162646_create_match_ff_mst_table', 1),
('2016_09_01_162647_create_achievment_mst_table', 1),
('2016_09_01_162648_create_leaderboard_mst_table', 1),
('2016_09_02_171131_create_arena_dtl_table', 1),
('2016_09_02_171132_create_arena_price_dtl_table', 1),
('2016_09_02_171133_create_team_plyr_dtl_table', 1),
('2016_09_02_171134_create_match_team_dtl_table', 1),
('2016_09_02_171135_create_team_image_dtl_table', 1),
('2016_09_02_171136_create_match_ff_plyr_dtl_table', 1),
('2016_09_02_171137_create_team_achievement_dtl', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `PLYR_MST`
--

CREATE TABLE IF NOT EXISTS `PLYR_MST` (
  `PLYR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PLYR_FIRST_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_LAST_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_GENDER` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_DOB` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_PHONE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_CITY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_FAVOURITE_NO` int(10) unsigned NOT NULL,
  `PLYR_EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_HEIGHT` int(10) unsigned NOT NULL,
  `PLYR_WEIGHT` int(10) unsigned NOT NULL,
  `PLYR_AGE` int(10) unsigned NOT NULL,
  `PLYR_BODY_BUILD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_PROFILE_IMG` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_FLAG_TEAM` tinyint(1) NOT NULL DEFAULT '0',
  `PLYR_FLAG_PRM` tinyint(1) NOT NULL DEFAULT '0',
  `PLYR_FLAG_ACTIVE` tinyint(1) NOT NULL DEFAULT '0',
  `PLYR_FLAG_INVITED` tinyint(1) NOT NULL DEFAULT '0',
  `PLYR_NUMBER` int(11) NOT NULL,
  `PLYR_ROLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PLYR_CONFIRMED` tinyint(1) NOT NULL DEFAULT '0',
  `PLYR_CONFIRMATION_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_ADMIN` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PLYR_ID`),
  UNIQUE KEY `plyr_mst_plyr_email_unique` (`PLYR_EMAIL`),
  KEY `plyr_mst_plyr_first_name_index` (`PLYR_FIRST_NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- Dumping data for table `PLYR_MST`
--

INSERT INTO `PLYR_MST` (`PLYR_ID`, `PLYR_FIRST_NAME`, `PLYR_LAST_NAME`, `PLYR_GENDER`, `PLYR_DOB`, `PLYR_PHONE`, `PLYR_CITY`, `PLYR_FAVOURITE_NO`, `PLYR_EMAIL`, `PLYR_PASSWORD`, `PLYR_HEIGHT`, `PLYR_WEIGHT`, `PLYR_AGE`, `PLYR_BODY_BUILD`, `PLYR_PROFILE_IMG`, `PLYR_FLAG_TEAM`, `PLYR_FLAG_PRM`, `PLYR_FLAG_ACTIVE`, `PLYR_FLAG_INVITED`, `PLYR_NUMBER`, `PLYR_ROLE`, `PLYR_CONFIRMED`, `PLYR_CONFIRMATION_CODE`, `IS_ADMIN`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Oran', 'Ankunding', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'ernest.glover@example.com', '$2y$10$loLlp7AC2xlLk4nYGDAaLepHUXpHScFEMNwg8P4mhudcLRtxnCM3W', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'qlYk4fXj6Kgal38K64MqvqnsW41muMJzcEzsQQbIVuuGTXyQprEZ6usGeHVx', '2016-10-07 19:29:46', '2016-10-09 05:20:43'),
(2, 'Laura', 'Adams', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'okeefe.frederick@example.com', '$2y$10$mxczKRMVRqLtyYWO8881..5dWlyb6zPJSuxGahZW7t2LeJSVV4wfu', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'KghvwTEpG1', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(3, 'Lia', 'Prohaska', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'deangelo68@example.net', '$2y$10$9dMoZ0d6W9lwMcWQDz09NOL.9mfNwk7jIKdDojcOiRcED8i0qZEea', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 1, 0, 0, 0, 0, '', 0, NULL, 0, '4g1x2OvRUbQo7aFZ94BolM3nZzy1Uv1d8BcIvF7qh0HxnvuPXHkQT7LWqBLk', '2016-10-07 19:29:46', '2016-10-09 06:50:06'),
(4, 'Cyril', 'Thiel', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'rickie11@example.net', '$2y$10$vBpyFRwgPvK0.65QbXU6leHcVMoLFXaf1AU9j0o2iMlHinrVECOxC', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'OJWeMNPez3', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(5, 'Claudine', 'McDermott', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'goldner.elissa@example.net', '$2y$10$LOzMsof9mAVH.Tqh03QVEeLq7E8QWrPTt4KXJfQpSJAqNcOJk7/MG', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'FQCafzrblz', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(6, 'Godfrey', 'Crist', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'jaskolski.antwon@example.net', '$2y$10$YWvMXYKst8UI0j2RzP2Nk.O0TXUSAHeSCCdo8zsJNK78f4.Iafjf2', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'R1onRjIFkG', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(7, 'Darby', 'Beahan', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'ycarter@example.org', '$2y$10$a0emlBqnB6PgghFICn4IEO1AyGP8oXpThcA2WTvgszu.jhVMinHay', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, '0RTB14dQJb', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(8, 'Ardith', 'Hand', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'thea.kulas@example.com', '$2y$10$6oOKRUUTXRAPgFhSLE9Mh.Z/RXkeN1LEuaUlbKVI9e.X5wXswfc0i', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'E0xw5Bvb3x', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(9, 'Hallie', 'Rempel', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'assunta.yost@example.net', '$2y$10$0NhWIxRYNjIYE5KoRY3BgefOwVimmLKgyxW.mxAKWAoi8hI9eZuyS', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'tECiKslMLg', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(10, 'Fred', 'Schulist', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'fisher.consuelo@example.com', '$2y$10$imetyNh4pNCTIXIj.7fk2ePQYy4C7lOdRAm1Syd2KdR8ATlbNN1zO', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'ks246no3C5', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(11, 'Marvin', 'Pouros', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'olindgren@example.com', '$2y$10$CHq6pNfFi7Tg/wL2wI3tKuDDgu/r.q.mzpYyaVwLQmKzCXvgw/eni', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'hRemM8Ci4t', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(12, 'Mitchel', 'Mraz', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'jennie.ernser@example.com', '$2y$10$NHK.Uo1KKfCnb9a1rkEJWuVFJsKgTJgP36AsV.DWebrN9NqV9iJOW', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'fnrcrnJ5fx', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(13, 'Ransom', 'Senger', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'hoppe.alta@example.net', '$2y$10$sd3dBqyik/gFlYeCM8ZNe.d2Q7q2nmeCvEJ1zx.o4mzB1f3FJ9j1e', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'hyOClApAC3', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(14, 'Dallas', 'Miller', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'dexter.schuster@example.net', '$2y$10$p9YAssWgeSB.mCmIqEwTFuI9VE4qpH.mBT2MqlZ372HaJo57wQNBG', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'D4zCh1jaie', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(15, 'Lenore', 'Roob', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'npurdy@example.org', '$2y$10$azlWgKCKPQpdowdwY2KGWu/6XXjAXQC/NVolznnpdRTsDOFZT8T2G', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'pCGD8jN5Lo', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(16, 'Eugenia', 'Parker', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'brando86@example.net', '$2y$10$OxO9xJK1pbqsdigok/uiEOxMlKFc1.stUoF4VKMDB0VjzC7IWdGby', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'xcZyD2qDfz', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(17, 'Virginia', 'Orn', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'cole84@example.net', '$2y$10$mFHfoAPK9h5XWKiF.SGa4eJBWeneElFaxaYvcDmMXnzwiP30pH5u2', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'FksYtWFX62', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(18, 'Olga', 'Carroll', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'estrella.brekke@example.com', '$2y$10$102OuU4QrvDDBdRl.b1DnuPEJmqKFCL//h91duOATZT9K6gaGueby', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'cmtu9nc8h2', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(19, 'Leonie', 'Tromp', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'stacey38@example.net', '$2y$10$aUsSao8bcE0BAuRkT09keOhRpgGabM00wwTh.rlzzf2/2e3xUrTUe', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'AMQ8QBe5DG', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(20, 'Alva', 'Cartwright', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'joanny56@example.net', '$2y$10$szy3zmY4pUIOSdhFeNlC3uUPOEsnvvZaVWvrZVBcXlbMJBDOfJ4eq', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'PQ225U9c6W', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(21, 'Jeramy', 'Hudson', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'juana.jacobson@example.com', '$2y$10$vxpvLOCXsMEEsdXZVXLBquwGsfPfXkX/wtPyBrAZMFoyAwuKp86he', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'p3d2ABGuM7', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(22, 'Elmore', 'Bauch', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'bernardo30@example.com', '$2y$10$Wj0kAJcyWUNTEAOinvKtHeIWXepq17Pjbvp.YL8HAfM2ZjOSshIHe', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'hjTysZweri', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(23, 'Muriel', 'Kuhn', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'hermiston.bettye@example.net', '$2y$10$EhpndqZJBWf6qm3n55X.hO4xVwvGQDaDXMPOH6Xno6hHL0PCHSbRK', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'teo4kzw0Z0', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(24, 'Michaela', 'Botsford', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'astrid79@example.com', '$2y$10$fG007itmirFES89IuV1hE.dVwQx1IKHSqGuy1wwyzBzm9W5MSRoxK', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, '8O1fK08jRO', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(25, 'Kattie', 'Terry', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'tyrique50@example.net', '$2y$10$4RUHHL6.mVKfU32X2f1Ag.4nZZvp1iDb1Ez5uiRfnjxOAZiSd5lqe', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'N0uTN6wxqu', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(26, 'Tom', 'Marks', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'malachi97@example.net', '$2y$10$YyMnoBbH4X4HG3RHMWfZoO8o.49e0rjdsKiJM9vebw0SA9oyD7giG', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'uChuKYH8DN', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(27, 'Neoma', 'Anderson', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'constantin.donnelly@example.org', '$2y$10$WTy3pzE6SqoaHK.S6Ay3M.f7KbWAOoAtV5iqW/bOXTg53OVGDtnQy', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'P9JlFpqrBp', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(28, 'Kolby', 'Beer', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'lakin.alvena@example.com', '$2y$10$089QYtl42KOXSDbUH7D94O61D1AZwRlswniBNVaB/9xdkkd.uk3pG', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'W4Rvt2Smoj', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(29, 'Catalina', 'Emard', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'herman.mabel@example.org', '$2y$10$yh.2EUFcAzJ8DjyJFUI1yuEy3W2u2FCB.Y/Jcmrk1R9adFnaj0fcm', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'uHRsmeUtl4', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(30, 'Gloria', 'Price', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'graynor@example.net', '$2y$10$ZERJPrSovXahW2indv91N.CBNg6DsYYIAilMwsdAhdkCfLSgYRT2q', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'NNYx8hzkaX', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(31, 'Waino', 'Boehm', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'dgerlach@example.com', '$2y$10$EdKCAbRfqe.HO/MDauQYxO3k49YbZQOSb/Rxhj87dKN.NiK8OHA3m', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, '9LMITbnRKk', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(32, 'Leatha', 'D''Amore', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'tzemlak@example.com', '$2y$10$POUeSNyw6C0.SxgoBS/.AuFZ2SVG540HpqFmnshrF97takSP3/MUS', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'S7dyIiHCLI', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(33, 'Cathryn', 'Prosacco', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'alindgren@example.org', '$2y$10$GSNDdllG8xC7DIyHFXsYveKgphOSZeC/Sv2vcy8zUrhczLLS0bLHi', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'uf2IdCN52y', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(34, 'Kirstin', 'Corwin', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'tillman.theresia@example.net', '$2y$10$sqt/YGTTZYYLRtb.iHiBnup3OafUEzUjtbIDNRmiXRl.Z0Cy6lViS', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'wRYvSnZCrq', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(35, 'Grover', 'Runte', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'arielle50@example.net', '$2y$10$8R.UiJrnFb9rUkFLGmhbqOXFNBd/pHawFoypWV38Uv9reKW9G4sJC', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'DmvF2wwn8e', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(36, 'Alessia', 'Kreiger', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'aurelie.sanford@example.com', '$2y$10$u.MYFlegDX9JQ6MLuNIGg.XSLOk2foM0s8/1fDTFFB007OYN74kbW', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'iKkM2lemVi', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(37, 'Mallie', 'Considine', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'nhudson@example.net', '$2y$10$EY6d.lDC5dG7ESOQGVkJJuWUDuhcS.JRHSLoeE24OqmlM9x8oUEf2', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'JbYmebRFae', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(38, 'Mireille', 'Dare', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'botsford.ned@example.org', '$2y$10$OOXidBN1FJgYJLCY2NUJeu9u0PjLPSnzY0jRhZfsTrzG0Eo7D3aOG', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, '2sg6cWXWmZ', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(39, 'Trycia', 'Kihn', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'noe06@example.net', '$2y$10$pyHV3FVlc.D.M4a5gDMXEOaMoiHjRz1NXHu4DS5.2onZIA8oNd9ES', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'ZrqNVdf7aa', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(40, 'Tristian', 'Stehr', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'domenica.schulist@example.org', '$2y$10$L.kkEW/Eulgj1dDVeV4jxOoudojYkh.0XSi/Gx6ZS3jpZDkEpMfBG', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'y4Gck2a8Gp', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(41, 'Jarod', 'Jerde', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'kmohr@example.org', '$2y$10$Y/UyW4.LUUzMmjWAnGDVB.Nrv5SfClwc2tFa7JhQneLY28s0zc9GK', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'OzhJYteYDY', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(42, 'Bertha', 'Krajcik', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'ebert.isabelle@example.com', '$2y$10$1cQM6rwRDSrNJoE.wimu1.B1WqyyF5OF7V4naSVh/.TSns7jgB9s.', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'nPlZ0YwnHk', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(43, 'Georgianna', 'Labadie', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'gusikowski.merl@example.net', '$2y$10$Z6sfe8AZvmu2UdxqfV6SReWTPg02yFgMHkuxlS4dZoATpwyFwkqo6', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'ihizKZre1L', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(44, 'Rosanna', 'Sporer', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'hauck.elbert@example.com', '$2y$10$GtKXUCLtuRDeJ7AtUpKKKu19N43QXom.JsLWBaGyy1aRx3Chm8Tsa', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'npeQgVh5WJ', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(45, 'Jordan', 'Kovacek', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'dwilliamson@example.net', '$2y$10$NhAlT/oztpyiL6FWUqEhlu/2c053zNKqfBkhkbZaF2p/5QmnLso/6', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'lubD6p8GAv', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(46, 'Eleanora', 'Yundt', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'retha.volkman@example.net', '$2y$10$/y/Pe3jHN7hi62ug6x6vieI3qv57sqUnS6PmQAd23QkIfl.SNuiVy', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'uMd4nY71mU', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(47, 'Toy', 'Monahan', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'delmer73@example.org', '$2y$10$FScVk2W1LukV0fCRBg244.y0zDCQbCf8Th7Z0azZaDR3q2iTaylk2', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'FxGjPk04oo', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(48, 'Maeve', 'Botsford', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'ekutch@example.org', '$2y$10$xpWBeiKbIQC/6sp646fRJ.Oc.QLkEbZ1cuZH13.8b85htJcICKVhm', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, '7HsKprxOk8', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(49, 'Buck', 'Maggio', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'gloria09@example.net', '$2y$10$eps/G6IZ/vItqTCSSAg5MODpk7HGgE24id1LtgVUcdP4AZ2qC6Uc6', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'UUFTKP9zrx', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(50, 'Ella', 'Satterfield', 'M', '07/02/1993', '0818661037', 'Jakarta', 7, 'willms.betsy@example.com', '$2y$10$0udl5Qz8OB8W6MGMKDmtDu.JhSCACuEp6KaLTehvYmwr6msLfgzBe', 0, 0, 0, '', 'img_player_201609241474684490596.jpg', 0, 0, 0, 0, 0, '', 0, NULL, 0, 'Ucut9VcDN6', '2016-10-07 19:29:46', '2016-10-07 19:29:46'),
(55, 'Gerry', 'Ganteng', 'Male', '10/09/2016', '', 'JAKARTA SELATAN', 7, 'gabrielcaesario@gmail.com', '$2y$10$iDYzATPU/Tcg8y.INmh.9.vSIrsdc9WxWlFGXiCM8HoLiZSRnMOg6', 0, 0, 0, '', 'img_player_201610091476025121475.png', 0, 0, 0, 0, 0, '', 1, NULL, 0, NULL, '2016-10-09 07:58:00', '2016-10-09 08:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `TEAM_ACHIEVEMENT_DTL`
--

CREATE TABLE IF NOT EXISTS `TEAM_ACHIEVEMENT_DTL` (
  `TEAM_ACHIEVEMENT_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEAM_ID` int(10) unsigned NOT NULL,
  `ACHIEVMENT_ID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TEAM_ACHIEVEMENT_DTL_ID`),
  KEY `team_achievement_dtl_team_id_foreign` (`TEAM_ID`),
  KEY `team_achievement_dtl_achievment_id_foreign` (`ACHIEVMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TEAM_IMAGE_DTL`
--

CREATE TABLE IF NOT EXISTS `TEAM_IMAGE_DTL` (
  `TEAM_IMAGE_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEAM_ID` int(10) unsigned NOT NULL,
  `IMAGE_ID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TEAM_IMAGE_DTL_ID`),
  KEY `team_image_dtl_team_id_foreign` (`TEAM_ID`),
  KEY `team_image_dtl_image_id_foreign` (`IMAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TEAM_MST`
--

CREATE TABLE IF NOT EXISTS `TEAM_MST` (
  `TEAM_MST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEAM_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEAM_LOGO` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEAM_GENDER` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEAM_DESC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEAM_CITY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TEAM_MST_ID`),
  KEY `team_mst_team_name_index` (`TEAM_NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `TEAM_MST`
--

INSERT INTO `TEAM_MST` (`TEAM_MST_ID`, `TEAM_NAME`, `TEAM_LOGO`, `TEAM_GENDER`, `TEAM_DESC`, `TEAM_CITY`, `created_at`, `updated_at`) VALUES
(2, 'tetst', 'img_team_201610081475895856993.png', 'on', 'test', 'Jakarta', '2016-10-07 20:04:17', '2016-10-07 20:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `TEAM_PLYR_DTL`
--

CREATE TABLE IF NOT EXISTS `TEAM_PLYR_DTL` (
  `TEAM_PLYR_DTL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEAM_ID` int(10) unsigned NOT NULL,
  `PLYR_ID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TEAM_PLYR_DTL_ID`),
  KEY `team_plyr_dtl_team_id_foreign` (`TEAM_ID`),
  KEY `team_plyr_dtl_plyr_id_foreign` (`PLYR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `TEAM_PLYR_DTL`
--

INSERT INTO `TEAM_PLYR_DTL` (`TEAM_PLYR_DTL_ID`, `TEAM_ID`, `PLYR_ID`, `created_at`, `updated_at`) VALUES
(5, 2, 3, '2016-10-07 20:04:17', '2016-10-07 20:04:17'),
(6, 2, 33, '2016-10-07 20:04:17', '2016-10-07 20:04:17'),
(7, 2, 35, '2016-10-07 20:04:17', '2016-10-07 20:04:17'),
(8, 2, 9, '2016-10-07 20:04:17', '2016-10-07 20:04:17');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ARENA_DTL`
--
ALTER TABLE `ARENA_DTL`
  ADD CONSTRAINT `arena_dtl_arena_id_foreign` FOREIGN KEY (`ARENA_ID`) REFERENCES `ARENA_MST` (`ARENA_ID`);

--
-- Constraints for table `ARENA_PRICE_DTL`
--
ALTER TABLE `ARENA_PRICE_DTL`
  ADD CONSTRAINT `arena_price_dtl_arena_dtl_id_foreign` FOREIGN KEY (`ARENA_DTL_ID`) REFERENCES `ARENA_DTL` (`ARENA_DTL_ID`);

--
-- Constraints for table `LEADERBOARD_MST`
--
ALTER TABLE `LEADERBOARD_MST`
  ADD CONSTRAINT `leaderboard_mst_team_id_foreign` FOREIGN KEY (`TEAM_ID`) REFERENCES `TEAM_MST` (`TEAM_MST_ID`);

--
-- Constraints for table `MATCH_FF_MST`
--
ALTER TABLE `MATCH_FF_MST`
  ADD CONSTRAINT `match_ff_mst_match_ff_created_by_foreign` FOREIGN KEY (`MATCH_FF_CREATED_BY`) REFERENCES `PLYR_MST` (`PLYR_ID`);

--
-- Constraints for table `MATCH_FF_PLYR_DTL`
--
ALTER TABLE `MATCH_FF_PLYR_DTL`
  ADD CONSTRAINT `match_ff_plyr_dtl_plyr_id_foreign` FOREIGN KEY (`PLYR_ID`) REFERENCES `PLYR_MST` (`PLYR_ID`),
  ADD CONSTRAINT `match_ff_plyr_dtl_match_ff_id_foreign` FOREIGN KEY (`MATCH_FF_ID`) REFERENCES `MATCH_FF_MST` (`MATCH_FF_MST_ID`);

--
-- Constraints for table `MATCH_MST`
--
ALTER TABLE `MATCH_MST`
  ADD CONSTRAINT `match_mst_match_created_by_foreign` FOREIGN KEY (`MATCH_CREATED_BY`) REFERENCES `PLYR_MST` (`PLYR_ID`),
  ADD CONSTRAINT `match_mst_match_loc_foreign` FOREIGN KEY (`MATCH_LOC`) REFERENCES `ARENA_MST` (`ARENA_ID`);

--
-- Constraints for table `MATCH_TEAM_DTL`
--
ALTER TABLE `MATCH_TEAM_DTL`
  ADD CONSTRAINT `match_team_dtl_team_id_foreign` FOREIGN KEY (`TEAM_ID`) REFERENCES `TEAM_MST` (`TEAM_MST_ID`),
  ADD CONSTRAINT `match_team_dtl_match_id_foreign` FOREIGN KEY (`MATCH_ID`) REFERENCES `MATCH_MST` (`MATCH_MST_ID`);

--
-- Constraints for table `TEAM_ACHIEVEMENT_DTL`
--
ALTER TABLE `TEAM_ACHIEVEMENT_DTL`
  ADD CONSTRAINT `team_achievement_dtl_achievment_id_foreign` FOREIGN KEY (`ACHIEVMENT_ID`) REFERENCES `ACHIEVEMENT_MST` (`ACHIEVEMENT_MST_ID`),
  ADD CONSTRAINT `team_achievement_dtl_team_id_foreign` FOREIGN KEY (`TEAM_ID`) REFERENCES `TEAM_MST` (`TEAM_MST_ID`);

--
-- Constraints for table `TEAM_IMAGE_DTL`
--
ALTER TABLE `TEAM_IMAGE_DTL`
  ADD CONSTRAINT `team_image_dtl_image_id_foreign` FOREIGN KEY (`IMAGE_ID`) REFERENCES `IMAGE_MST` (`IMAGE_MST_ID`),
  ADD CONSTRAINT `team_image_dtl_team_id_foreign` FOREIGN KEY (`TEAM_ID`) REFERENCES `TEAM_MST` (`TEAM_MST_ID`);

--
-- Constraints for table `TEAM_PLYR_DTL`
--
ALTER TABLE `TEAM_PLYR_DTL`
  ADD CONSTRAINT `team_plyr_dtl_plyr_id_foreign` FOREIGN KEY (`PLYR_ID`) REFERENCES `PLYR_MST` (`PLYR_ID`),
  ADD CONSTRAINT `team_plyr_dtl_team_id_foreign` FOREIGN KEY (`TEAM_ID`) REFERENCES `TEAM_MST` (`TEAM_MST_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
