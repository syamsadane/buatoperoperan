<div class="container">
	<div class="arenaList-section row">
		<div class="search-wrap pull-left">
			<div class="search-top">
				<div class="m-searchInput row">
					<input type="text" class="pull-left" placeholder="Search...">
					<button class="pull-left"><i class="lnr lnr-magnifier"></i></button>
				</div>
			</div>
			<div class="search-bottom">
				<div class="form-section">
					<label>City</label>
					<select class="select chosen-select">
						<option value="" class="hideit" selected>City name</option>
						<option value="">Jakarta</option>
						<option value="">Bandung</option>
						<option value="">Bali</option>
					</select>
				</div>
				<div class="form-section">
					<label>Harga</label>
					<input type="text" class="form-rangeInput">
				</div>
				<div class="form-section">
					<label>Arena type</label>
					<div class="row">
						<div class="radio-wrap pull-left" data-name="tipe-arena2">
							<div class="radio-box row">
								<span></span>
								<p>Indoor</p>
							</div>
							<input type="radio" name="tipe-arena2">
						</div>
						<div class="radio-wrap pull-left" data-name="tipe-arena2">
							<div class="radio-box row">
								<span></span>
								<p>Outdoor</p>
							</div>
							<input type="radio" name="tipe-arena2">
						</div>
					</div>
				</div>
				<div class="form-section">
					<label>Arena type</label>
					<div class="row">
						<div class="radio-wrap pull-left" data-name="tipe-arena">
							<div class="radio-box row">
								<span></span>
								<p>Rumput</p>
							</div>
							<input type="radio" name="tipe-arena">
						</div>
						<div class="radio-wrap pull-left" data-name="tipe-arena">
							<div class="radio-box row">
								<span></span>
								<p>Semen</p>
							</div>
							<input type="radio" name="tipe-arena">
						</div>
						<div class="radio-wrap pull-left" data-name="tipe-arena">
							<div class="radio-box row">
								<span></span>
								<p>Aspal</p>
							</div>
							<input type="radio" name="tipe-arena">
						</div>
						<div class="radio-wrap pull-left" data-name="tipe-arena">
							<div class="radio-box row">
								<span></span>
								<p>Magma</p>
							</div>
							<input type="radio" name="tipe-arena">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="result-wrap pull-left">
			<div class="arena-item row">
				<div class="image-wrap">
					<div class="r-image">
						<div class="inner-image"><div class="bg-image" style="background-image:url(images/jpg/Img-Futsal03.jpg);"></div></div>
					</div>
				</div>
				<div class="desc-wrap">
					<div class="tittle">Futsal kura - kura ninja</div>
					<div class="address">Jln. Tangerang Selatan 2, No. 39</div>
					<div class="city">Jakarta Timur</div>
				</div>
				<div class="button-wrap">
					<div class="price">Rp. 150.000</div>
					<a href="" class="m-button blue">View</a>
				</div>
			</div>

			<div class="arena-item row">
				<div class="image-wrap">
					<div class="r-image">
						<div class="inner-image"><div class="bg-image" style="background-image:url(images/jpg/Img-Futsal03.jpg);"></div></div>
					</div>
				</div>
				<div class="desc-wrap">
					<div class="tittle">Futsal kura - kura ninja</div>
					<div class="address">Jln. Tangerang Selatan 2, No. 39</div>
					<div class="city">Jakarta Timur</div>
				</div>
				<div class="button-wrap">
					<div class="price">Rp. 150.000</div>
					<a href="" class="m-button blue">View</a>
				</div>
			</div>

			<div class="arena-item row">
				<div class="image-wrap">
					<div class="r-image">
						<div class="inner-image"><div class="bg-image" style="background-image:url(images/jpg/Img-Futsal03.jpg);"></div></div>
					</div>
				</div>
				<div class="desc-wrap">
					<div class="tittle">Futsal kura - kura ninja</div>
					<div class="address">Jln. Tangerang Selatan 2, No. 39</div>
					<div class="city">Jakarta Timur</div>
				</div>
				<div class="button-wrap">
					<div class="price">Rp. 150.000</div>
					<a href="" class="m-button blue">View</a>
				</div>
			</div>

			<div class="arena-item row">
				<div class="image-wrap">
					<div class="r-image">
						<div class="inner-image"><div class="bg-image" style="background-image:url(images/jpg/Img-Futsal03.jpg);"></div></div>
					</div>
				</div>
				<div class="desc-wrap">
					<div class="tittle">Futsal kura - kura ninja</div>
					<div class="address">Jln. Tangerang Selatan 2, No. 39</div>
					<div class="city">Jakarta Timur</div>
				</div>
				<div class="button-wrap">
					<div class="price">Rp. 150.000</div>
					<a href="" class="m-button blue">View</a>
				</div>
			</div>

			<div class="oper-paging">
				<ul>
					<li><a href="">Prev</a></li>
					<li><a href="">1</a></li>
					<li><a href="" class="active">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">5</a></li>
					<li><a href="">Next</a></li>
				</ul>
			</div>
			
		</div>
	</div>
</div>

<script src="js/views/arena.js"></script>