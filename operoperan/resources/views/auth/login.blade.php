@extends('layout.index')



@section('content')

	@include('layout.message.message')
<div class="background-color lightGrey">
	<div class="container">
		<div class="sign-wrap">
			<div class="tabs-wrap">
				<a href="#login" class="active">Login</a>
				<a href="#register">Register</a>
			</div>
			<div class="content-wrap">
			{!! Form::open(array('route' => 'auth.login', 'method' => 'POST')) !!}
				<div id="login" class="form-wrap">
					<div class="form-section {{$errors->has('PLYR_EMAIL') ? 'has-error' : ''}}">
						<label>Email</label>
						<input type="text" name="PLYR_EMAIL">
						@if($errors->has('email'))
						    <div class="error-state">Email Wrong</div> {{ $errors->first('PLYR_EMAIL') }}
						@endif
					</div>

					<div class="form-section">
						<label>Password</label>
						<input type="password" name="password">
						<div class="error-state">Password Wrong</div>
					</div>
					<div class="row">
						<div class="pull-left"></div>
						<div class="pull-right"><a href="#" id="forgotButton">Forgot password?</a></div>
					</div>

					<div class="form-section {{Session::has('error') ? 'has-error' : ''}}">
						<div class="error-state">{!! Session::get('error') !!}</div>
					</div>

					<div class="form-section parentcenter">
						<button class="m-button blue" name="btnLogin">Login</button>
					</div>
				</div>
			{!! Form::close() !!}

			{!! Form::open(array('route' => 'auth.register', 'method' => 'POST')) !!}
				<div id="register" class="form-wrap hideit">
					<div class="form-section row {{$errors->has('firstName') ? 'error' : ''}}">
						<div class="half error">
							<label>First Name</label>
							<input type="text" name="firstName" value="{{ old('firstName') }}">
							@if($errors->has('firstName'))
								<div class="error-state">{{ $errors->first('firstName') }}</div>
							@endif
						</div>
						<div class="half {{$errors->has('lastName') ? 'error' : ''}}">
							<label>Last Name</label>
							<input type="text" name="lastName" value="{{ old('lastName') }}">
							@if($errors->has('lastName'))
								<div class="error-state">{{ $errors->first('lastName') }}</div>
							@endif
						</div>
					</div>
					<div class="form-section {{ $errors->has('registerEmail') ? 'error' : '' }}">
						<label>Email</label>
						<input type="text" name="registerEmail" value="{{ old('registerEmail') }}">
						@if($errors->has('registerEmail'))
							<div class="error-state">{{ $errors->first('registerEmail') }}</div>
						@endif
					</div>
					<div class="form-section {{ $errors->has('registerPassword') ? 'error' : '' }}">
						<label>Password</label>
						<input type="password" name="registerPassword" value="{{ old('registerPassword') }}">
						@if($errors->has('registerPassword'))
							<div class="error-state">{{ $errors->first('registerPassword') }}</div>
						@endif
					</div>
					<div class="form-section {{ $errors->has('confirmPassword') ? 'error' : '' }}">
						<label>Confirm Password</label>
						<input type="password" name="confirmPassword" value="">
						@if($errors->has('confirmPassword'))
							<div class="error-state">{{ $errors->first('confirmPassword') }}</div>
						@endif
					</div>
					<div class="form-section {{ $errors->has('gender') ? 'error' : '' }}">
						<label>Gender</label>
						<div class="row">
							<div class="radio-wrap pull-left" data-name="gender">
								<div class="radio-box row">
									<span></span>
									<p>Male</p>
								</div>
								<input type="radio" name="gender" value="Male">
							</div>
							<div class="radio-wrap pull-left" data-name="gender">
								<div class="radio-box row">
									<span></span>
									<p>Female</p>
								</div>
								<input type="radio" name="gender" value="Female">
							</div>
						</div>
						@if($errors->has('gender'))
							<div class="error-state">{{ $errors->first('gender') }}</div>
						@endif
					</div>
					<div class="form-section {{ $errors->has('birthDate') ? 'error' : '' }}">
						<label>Birth Date</label>
						<input type="text" name="birthDate" class="m-datepicker" value="{{ old('birthDate') }}">
						@if($errors->has('birthDate'))
							<div class="error-state">{{ $errors->first('birthDate') }}</div>
						@endif
					</div>
					<div class="form-section parentcenter">
						<button class="m-button blue" name="btnRegister">Register</button>
					</div>
				</div>

			{!! Form::close() !!}

			{!! Form::open(array('route' => 'auth.resetPassword', 'method' => 'POST')) !!}
				<div id="forgotpass" class="form-wrap hideit">
					<div class="form-section">
						<label>Your Email</label>
						<input type="text" name="email">
						<div class="error-state">Email Wrong</div>
					</div>
					<div class="form-section parentcenter">
						<button class="m-button blue">Send</button>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>


		<div class="sign-wrap2 hideit">
			<div class="l-tittle blue"><span>Personal Information</span></div>
			<div class="inner">
				<div class="row">
					<div class="half">
						<div class="uploadPhoto-section">
							<div class="image-null md"></div>
							<div class="image-browse">
								<input type="file" id="file-img">
								<label for="file-img" class="row">
									<strong class="pull-right">
										<div class="icon-text">
											<i class="lnr lnr-download"></i>
											<span>Download</span>
										</div>
									</strong>
									<p class="pull-right">&nbsp;</p>
								</label>
							</div>
						</div>
					</div>
					<div class="half">
						<div class="form-section">
							<label>City</label>
							<input type="text">
							<div class="error-state"></div>
						</div>
						<div class="form-section">
							<label>Favorite Number</label>
							<input type="text">
							<div class="error-state"></div>
						</div>
					</div>
				</div>
				<div class="parentright">
					<button class="m-button blue">Finish</button>
				</div>
			{!! Form::close() !!}
			</div>

		</div>
	</div>
</div>

<script type="text/javascript" src="{{asset('js/views/login.js')}}"></script>

@endsection