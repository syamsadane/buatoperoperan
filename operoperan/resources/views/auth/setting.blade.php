@extends('layout.index')


@section('content')

    @include('layout.message.message')
    <div class="setting-section">
        <div class="container">
            <div class="tab-setting">
                <div class="m-tittle">Setting.</div>
                <!-- Begin Tabs -->
                <div class="m-allTabs">
                    <div class="tabs-wrap">
                        <ul>
                            <li><a href="#MyPassword" class="yellow active">My Password</a></li>
                            <li><a href="#MyProfile" class="yellow">My Profile</a></li>
                        </ul>
                    </div>
                </div><!-- end Tabs -->


                <!-- begin MyPassword -->

                <div id="MyPassword" class="t-allTabs">
                    {!! Form::open(array('route' => 'auth.update.password', 'method' => 'POST')) !!}
                    <div class="border-setting">
                        <div class="form-section {{ $errors->has('oldPassword') ? 'error' : '' }}">
                            <label>Old Password</label>
                            <input type="text" name="oldPassword">
                            @if($errors->has('oldPassword'))
                                <div class="error-state">{{ $errors->first('oldPassword') }}</div>
                            @endif
                        </div>

                        <div class="form-section {{ $errors->has('newPassword') ? 'error' : '' }}">
                            <label>New Password</label>
                            <input type="text" name="newPassword">
                            @if($errors->has('newPassword'))
                                <div class="error-state">{{ $errors->first('newPassword') }}</div>
                            @endif
                        </div>

                        <div class="form-section {{ $errors->has('confirmPassword') ? 'error' : '' }}">
                            <label>Confirm Password</label>
                            <input type="text" name="confirmPassword">
                            @if($errors->has('confirmPassword'))
                                <div class="error-state">{{ $errors->first('confirmPassword') }}</div>
                            @endif
                        </div>

                        <div class="form-section pull-right">
                            <button class="m-button blue">Finish</button>
                        </div>
                        {!! Form::close() !!}
                    </div> <!-- end of border setting-->

                </div><!-- end of MyPassword -->


                <!-- begin test2 -->
                {!! Form::open(array('route' => 'auth.post.settings', 'method' => 'POST', 'files' => 'true')) !!}
                <div id="MyProfile" class="hideit t-allTabs">
                    <div class="border-setting">
                        <div class="s-tittle"><b>Edit Profile.</b></div>
                        <div class="form-section row">
                            <div class="half" {{ $errors->has('firstName') ? 'error' : '' }}>
                                <label>First Name</label>
                                <input type="text" disabled value="{{Auth::user()->PLYR_FIRST_NAME }}" name="firstName">
                                @if($errors->has('firstName'))
                                    <div class="error-state">{{ $errors->first('firstName') }}</div>
                                @endif
                            </div>
                            <div class="half">
                                <label>Last Name</label>
                                <input type="text" disabled value="{{Auth::user()->PLYR_LAST_NAME }}" name="lastName">

                                <div class="error-state">Email Wrong</div>
                            </div>
                        </div>

                        <div class="form-section">
                            <label>Email</label>
                            <input type="text" disabled value="{{Auth::user()->PLYR_EMAIL }}">

                            <div class="error-state">Password Wrong</div>
                        </div>

                        <div class="form-section">
                            <label>Birth Date</label>
                            <input type="text" disabled value="{{Auth::user()->PLYR_DOB }}" name="birthDate">

                            <div class="error-state">Password Wrong</div>
                        </div>

                        <div class="form-section">
                            <label>City</label>
                            <input type="text" placeholder="Choose Your Hometown" value="{{Auth::user()->PLYR_CITY}}"
                                   name="city">

                            <div class="error-state">Password Wrong</div>
                        </div>

                        <div class="form-section {{ $errors->has('phoneNumber') ? 'error' : ''}}">
                            <label>Phone Number</label>
                            <input type="text" placeholder="put your phone number" value="{{Auth::user()->PLYR_PHONE}}"
                                   name="phoneNumber">
                            @if($errors->has('phoneNumber'))
                                <div class="error-state">{{ $errors->first('phoneNumber') }}</div>
                            @endif
                        </div>

                        <div class="form-section {{ $errors->has('favouriteNo') ? 'error' : '' }}">
                            <label>Favorite Number</label>
                            <input type="text" value="{{Auth::user()->PLYR_FAVOURITE_NO }}" name="favouriteNo">
                            @if($errors->has('favouriteNo'))
                                <div class="error-state">{{ $errors->first('favouriteNo') }}</div>
                            @endif
                        </div>

                        <hr>

                        <div class="s-tittle"><b>Profile Picture.</b></div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="m-image md"><img src="/image/jpg/{{Auth::user()->PLYR_PROFILE_IMG }}"
                                                             alt=""></div>
                            </div> <!--end of col-md-6-->
                            <div class="col-md-6">
                                <div class="uploadPhoto-section">
                                    <div class="image-browse-setting">
                                        <input type="file" id="file-img" name="image">
                                        <label for="file-img" class="row">
                                            <strong class="pull-right">
                                                <div class="icon-text">
                                                    <i class="lnr lnr-browse"></i>
                                                    <span>Browse</span>
                                                </div>
                                            </strong>

                                            <p class="pull-right">&nbsp;</p>
                                        </label>
                                    </div>

                                    <div class="parentleft">
                                        <a href="#" class="l-button blue">Select photo</a>
                                    </div>
                                </div>
                            </div>
                        </div><!--end of row-->

                        <hr>
                        @if(Auth::user()->PLYR_FLAG_TEAM == 1)
                            <div class="s-tittle"><b>Team.</b></div>
                            <div>
                                <div class="team-fill row">
                                    <div class="col-md-5">
                                        <div class="m-image sm no-border"><img src="images/jpg/team-logo.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-7 team-setting">
                                        <div class="pull-left">
                                            <div class="tittle">Mafia Migas FC</div>
                                            <div class="city">Jakarta Selatan</div>
                                            <a href="#" class="l-button blue">Leave</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="form-section pull-right">
                            <button class="m-button blue">Finish</button>
                        </div>
                        {!! Form::close() !!}
                    </div><!-- end of border-setting-->
                </div> <!-- End - MyProfile -->


            </div><!-- end of tab-setting-->


        </div> <!-- end of container -->
    </div> <!-- end of setting -->
@endsection

