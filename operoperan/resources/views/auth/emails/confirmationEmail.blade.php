<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Verify Your Email Address</h2>

<div>
    Thanks for creating an account at <a href="http://www.operoperan.xyz">Operoperan.xyz</a>
    Please follow the link below to verify your email address<br/>
    <a href="{{ URL::to('operoperan-verify/' . $confirmationCode) }}" target="_blank">{{ URL::to('operoperan-verify/' . $confirmationCode) }}</a><br/>
</div>

</body>
</html>