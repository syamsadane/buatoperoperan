@extends('layout.index')

@section('content')
    <h1>{!! $playerName !!} invites you to join <a href="http://www.operoperan.xyz" target="_blank">operoperan.xyz</a></h1>

    <script type="text/javascript">
        $(document).ready(function (e) {
            $('.menu-wrap').hide();
            $('.nav-wrap').hide();
        });
    </script>
@endsection