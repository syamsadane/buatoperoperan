@extends('layout.index')

@if(!empty($plyr_id))
    {{ \Illuminate\Support\Facades\Session::flash('PLYR_ID', $plyr_id) }}
@endif

@include('layout.message.message')

@section('content')
    <div class="sign-wrap2">
        {!! Form::open(array('route' => 'auth.insertPersonalInfo', 'method' => 'POST', 'files' => 'true')) !!}
        <div class="l-tittle blue"><span>Personal Information</span></div>
        <div class="inner">
            <div class="row">
                <div class="half">
                    <div class="uploadPhoto-section {{$errors->has('image') ? 'error' : ''}}">
                        <div class="image-null md"></div>
                        <div class="image-browse">
                            <input type="file" id="file-img" name="image">
                            @if($errors->has('image'))
                                <div class="error-state"> {{ $errors->first('image') }}</div>
                            @endif
                            <label for="file-img" class="row">
                                <strong class="pull-right">
                                    <div class="icon-text">
                                        <i class="lnr lnr-download"></i>
                                        <span>Download</span>
                                    </div>
                                </strong>

                                <p class="pull-right">&nbsp;</p>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="half">
                    <div class="form-section {{ $errors->has('city') ? 'error' : '' }}">
                        <label>City</label>
                        <input type="text" name="city">
                        @if($errors->has('city'))
                            <div class="error-state"> {{ $errors->first('city') }}</div>
                        @endif
                    </div>
                    <div class="form-section {{ $errors->has('favouriteNumber') ? 'error' : '' }}">
                        <label>Favorite Number</label>
                        <input type="text" name="favouriteNumber">
                        @if($errors->has('favouriteNumber'))
                            <div class="error-state"> {{ $errors->first('favouriteNumber') }}</div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="parentright">
                <button class="m-button blue">Finish</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
@endsection