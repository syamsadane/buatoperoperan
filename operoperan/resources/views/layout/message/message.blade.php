@if(Session::has('success'))
    <div id="modal-successBox" class="active m-modals statusMessage">
        <div class="background-wrap"> </div>
        <div class="modal-wrap">
            <!-- <div class="tittle">Error!</div> -->
            <div class="body">
                <div class="single-icon success">
                    <i class="lnr lnr-smile"></i>
                </div>
                <div class="desc lg parentcenter">Success!</div>
                <div class="desc sm parentcenter">{{Session::get('success')}}</div>
            </div>
        </div>
    </div>
@endif

@if(Session::has('error'))
    <div id="modal-successBox" class="active m-modals statusMessage">
        <div class="background-wrap"> </div>
        <div class="modal-wrap">
            <!-- <div class="tittle">Error!</div> -->
            <div class="body">
                <div class="single-icon failed">
                    <i class="lnr lnr-sad"></i>
                </div>
                <div class="desc lg parentcenter">Error!</div>
                <div class="desc sm parentcenter">{{Session::get('error')}}</div>
            </div>

        </div>
    </div>
@endif


<script>
    window.setTimeout(function() {
        $(".statusMessage").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 2000);
</script>