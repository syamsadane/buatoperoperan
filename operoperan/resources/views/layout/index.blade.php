<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('js/vendor/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/font-awesome.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/chosen.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/dropzone.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/justifiedGallery.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/selectize.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/jquery.dataTables.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/jquery.range.css')}}"/>
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="{{asset('css/global.css')}}">
    <script src="{{asset('js/vendor/modernizr-2.6.2.min.js')}}"></script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Noto+Sans:400,400i,700,700i"
          rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('js/vendor/jquery-ui.min.js')}}"></script>

    <script>
        var functions = new Array();
    </script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->
@if(Auth::guest())
    <div id="header" class="">
        <div class="row">
            <div class="logo pull-left">
                <a href="{{ route('home') }}"> <img src="{{ asset('image/png/logo.png') }}" alt=""> </a>
            </div>
            <div class="menu-wrap">
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="">Klasemen</a></li>
                    <li><a href="{{ route('search')}}">Search</a></li>
                    <li><a href="{{ route('arena')}}">Arena</a></li>
                    <li><a href="">Blog</a></li>
                </ul>
            </div>
            <div class="nav-wrap pull-right">
                <a href="{{ route('auth.login') }}"> Masuk / Daftar </a>
            </div>
        </div>
    </div>
@else
    <div id="header" class="header-login">
        <div class="row">
            <div class="logo pull-left">
                <a href="{{ route('home') }}"> <img src="{{ asset('image/png/logo.png') }}" alt=""> </a>
            </div>
            <div class="menu-wrap">
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="">Klasemen</a></li>
                    <li><a href="">Search</a></li>
                    <li><a href="">Arena</a></li>
                    <li><a href="">Blog</a></li>
                </ul>
            </div>
            <div class="userNav-wrap pull-right">
                <ul class="row">
                    <li>
                        <a href="#" class="notification"><i class="lnr lnr-alarm"></i></a>
                        <ul class="dropdown-head">
                            <li>
                                <a href="#">
                                    <div class="notification-box row">
                                        <div class="point black pull-left"><span></span></div>
                                        <div class="desc-wrap pull-left">
                                            <div class="tittle">Lorem ipsum dolor sit amet, consectetur adipisicing
                                                elit.
                                            </div>
                                            <div class="date">2 days ago</div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="user-head">
                            <img src="/image/jpg/{{Auth::user()->PLYR_PROFILE_IMG}}" alt="">
                            <span> {!! Auth::user()->PLYR_FIRST_NAME.' '. Auth::user()->PLYR_LAST_NAME !!}</span>
                        </a>
                        <ul class="dropdown-head">
                            <li>
                                <a href="#">
                                    <div class="icon-text black"><i class="lnr lnr-sun"></i><span>My Match</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('player.detail') }}">
                                    <div class="icon-text black"><i class="lnr lnr-user"></i><span>Profile</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="">
                                    <div class="icon-text black"><i class="lnr lnr-users"></i><span>Team</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('auth.settings') }}">
                                    <div class="icon-text black"><i class="lnr lnr-cog"></i><span>Settings</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('auth.logout')}}">
                                    <div class="icon-text black"><i class="lnr lnr-power-switch"></i><span>Logout</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="content">
    @yield('content')
</div>

<div id="footer" class="row">
    <div class="foot-top">
        <div class="container">
            <div class="link-wrap pull-right">
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">Privacy & Policies</a></li>
                </ul>
            </div>
            <div class="logo">
                <img src="{{asset('image/png/logo-white.png')}}" alt="">
            </div>
        </div>
    </div>
    <div class="foot-bottom">
        <div class="container">
            <div class="row">
                <div class="sosmed-wrap pull-left">
                    <ul>
                        <li><a href="#"><img src="{{asset('image/svg/facebook-logo.svg')}}" class="svg-image"
                                             alt=""></a></li>
                        <li><a href="#"><img src="{{asset('image/svg/youtube-logo.svg')}}" class="svg-image" alt=""></a>
                        </li>
                        <li><a href="#"><img src="{{asset('image/svg/google-logo.svg')}}" class="svg-image" alt=""></a>
                        </li>
                    </ul>
                </div>
                <div class="copyright pull-right">
                    oper2an@copyright
                </div>
            </div>
        </div>
    </div>
</div>


<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script type="text/javascript" src="{{asset('js/vendor/slick/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/chosen.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/combodate.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/masonry.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/microplugin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/sifter.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/selectize.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/jquery.zoom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/jquery.justifiedGallery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/jquery.panzoom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/dropzone.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vendor/jquery.range-min.js')}}"></script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/global.js')}}"></script>
{{--<script src="enginejs/config/config.js"></script>--}}
{{--<script src="enginejs/list.js"></script>--}}
{{--<script src="enginejs/template.js"></script>--}}

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');
</script>
<script>
    $(".userNav-wrap ul li a").on("click", function (e) {

        if (!$(this).parents("li").find(".dropdown-head").hasClass("active")) {
            $(".userNav-wrap ul li a").parents("li").find(".dropdown-head").removeClass("active");
        }
        $(this).parents("li").find(".dropdown-head").toggleClass("active");
    });
</script>
</body>
</html>
