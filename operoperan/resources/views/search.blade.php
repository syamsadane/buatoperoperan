@extends('layout.index')

@section('content')
<div class="container">
	<div class="m-allTabs">
		<div class="tabs-wrap">
			<ul>
				<li><a href="#playerList" class="active yellow">Player</a></li>
				<li><a href="#teamList" class="yellow">Team</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="container">
	<div id="playerList" class="result-search t-allTabs">
		<div class="search-wrap">
			<div class="inner-search">
				<div class="m-searchInput row">
					<input type="text" class="pull-left">
					<button class="pull-left"><i class="lnr lnr-magnifier"></i></button>
				</div>
			</div>
			<div class="filter-search">
				<div class="form-section row">
					<div class="half">
						<label>City</label>
						<select class="select" name="" id="">
							<option value="Jakarta">Jakarta</option>
						</select>
					</div>
					<div class="half">
						<label>Gender</label>
						<div class="row">
							<div class="radio-wrap pull-left" data-name="gender">
								<div class="radio-box row">
									<span></span>
									<p>Man</p>
								</div>
								<input type="radio" name="gender">
							</div>
							<div class="radio-wrap pull-left" data-name="gender">
								<div class="radio-box row">
									<span></span>
									<p>Female</p>
								</div>
								<input type="radio" name="gender">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="l-tittle blue"><span>Result "Arya"</span></div>
		<div class="row">
			<div class="col-md-6 no-padding result-item">
				<div class="player-visit row">
					<div class="m-image xs pull-left"><img src="images/jpg/profpic1.jpg" alt=""></div>
					<div class="bio pull-left">
						<p>Aryatama Krishna</p>
						<p>Mafia Migas FC</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 no-padding result-item">
				<div class="player-visit row">
					<div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
					<div class="bio pull-left">
						<p>Aryatama Krishna</p>
						<p>Mafia Migas FC</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 no-padding result-item">
				<div class="player-visit row">
					<div class="m-image xs pull-left"><img src="images/jpg/profpic2.jpg" alt=""></div>
					<div class="bio pull-left">
						<p>Aryatama Krishna</p>
						<p>Mafia Migas FC</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 no-padding result-item">
				<div class="player-visit row">
					<div class="m-image xs pull-left"><img src="images/jpg/profpic1.jpg" alt=""></div>
					<div class="bio pull-left">
						<p>Aryatama Krishna</p>
						<p>Mafia Migas FC</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 no-padding result-item">
				<div class="player-visit row">
					<div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
					<div class="bio pull-left">
						<p>Aryatama Krishna</p>
						<p>Mafia Migas FC</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 no-padding result-item">
				<div class="player-visit row">
					<div class="m-image xs pull-left"><img src="images/jpg/profpic2.jpg" alt=""></div>
					<div class="bio pull-left">
						<p>Aryatama Krishna</p>
						<p>Mafia Migas FC</p>
					</div>
				</div>
			</div>
		</div>
		<div class="oper-paging">
			<ul>
				<li><a href="">Prev</a></li>
				<li><a href="">1</a></li>
				<li><a href="" class="active">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li><a href="">5</a></li>
				<li><a href="">Next</a></li>
			</ul>
		</div>
	</div>
	<div id="teamList" class="result-search t-allTabs hideit">
		<div class="search-wrap">
			<div class="inner-search">
				<div class="m-searchInput row">
					<input type="text" class="pull-left">
					<button class="pull-left"><i class="lnr lnr-magnifier"></i></button>
				</div>
			</div>
			<div class="filter-search">
				<div class="form-section row">
					<div class="half">
						<label>City</label>
						<select class="select" name="" id="">
							<option value="Jakarta">Jakarta</option>
						</select>
					</div>
					<div class="half">
						<label>Gender</label>
						<div class="row">
							<div class="radio-wrap pull-left" data-name="gender">
								<div class="radio-box row">
									<span></span>
									<p>Man</p>
								</div>
								<input type="radio" name="gender">
							</div>
							<div class="radio-wrap pull-left" data-name="gender">
								<div class="radio-box row">
									<span></span>
									<p>Female</p>
								</div>
								<input type="radio" name="gender">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="l-tittle blue"><span>Result "Arya"</span></div>
		<div class="row">
			<div class="col-md-3">
				<div class="team-visit">
					<div class="img-wrap">
						<img src="images/jpg/team-logo.jpg" alt="">
					</div>
					<div class="tittle">Mafia Migas FC</div>
					<div class="city">Jakarta</div>
					<a href="" class="m-button blue">Visit</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team-visit">
					<div class="img-wrap">
						<img src="images/jpg/team-logo.jpg" alt="">
					</div>
					<div class="tittle">Mafia Migas FC</div>
					<div class="city">Jakarta</div>
					<a href="" class="m-button blue">Visit</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team-visit">
					<div class="img-wrap">
						<img src="images/jpg/team-logo.jpg" alt="">
					</div>
					<div class="tittle">Mafia Migas FC</div>
					<div class="city">Jakarta</div>
					<a href="" class="m-button blue">Visit</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team-visit">
					<div class="img-wrap">
						<img src="images/jpg/team-logo.jpg" alt="">
					</div>
					<div class="tittle">Mafia Migas FC</div>
					<div class="city">Jakarta</div>
					<a href="" class="m-button blue">Visit</a>
				</div>
			</div>
		</div>
		<div class="oper-paging">
			<ul>
				<li><a href="">Prev</a></li>
				<li><a href="">1</a></li>
				<li><a href="" class="active">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li><a href="">5</a></li>
				<li><a href="">Next</a></li>
			</ul>
		</div>
	</div>
</div>
@endsection