<!-- Begin - Image Banner -->
<div class="team-banner">
	<div class="banner-image">
		<div class="inner-image">
			<img src="images/jpg/Img-Futsal01.jpg" alt="">
		</div>
	</div>
</div>
<!-- End - Image Banner -->

<!-- Begin - Bio Arena Detail -->
<div class="teamDetail-bio">
	<div class="container">
		<div class="row">
			<div class="inner-bio pull-left row">
				<div class="pic-wrap pull-left">
					<div class="r-image">
						<div class="inner-image">
							<img src="images/jpg/team-logo.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="bio-wrap pull-left">
					<div class="tittle">Mafia Migas FC</div>
					<div class="city">Jakarta Selatan</div>
					<div class="status">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi eveniet, cumque pariatur. Numquam, in dignissimos eaque provident eius ipsa cupiditate mollitia amet eveniet voluptate itaque! Adipisci, dolorum aperiam quas quod.</div>
				</div>
			</div>
			<div class="inner-bio pull-right row">
				<div class="ranking-wrap pull-right">
					<p>Ranking</p>
					<p>9217</p>
				</div>
				<div class="menu-wrap pull-right">
					<ul>
						<li><a href="#">Profile</a></li>
						<li><a href="#">Gallery</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End - Bio Arena Detail -->

<div class="teamGallery-section">
	<div class="container">
<!-- Begin - Menu Gallery -->
		<div class="teamGallery-menu row">
			<ul class="pull-left row">
				<li class="pull-left"><a href="#allPhotos">All photos</a></li>
				<li class="pull-left"><a href="#albumPhotos">Album</a></li>
			</ul>
			<div class="create-wrap parentright pull-right">
				<a href="#" class="icon-text grey modals-trigger" data-target="#modal-createAlbum"><i class="lnr lnr-plus-circle"></i><span>Create New Album</span></a>
			</div>
		</div>
<!-- End - Menu Gallery -->

<!-- Begin - Gallery All -->
		<div class="teamGallery-desc">
			<h3></h3>
			<div class="location"></div>
			<div class="story"></div>
		</div>
		<div class="teamGallery-list">
			<div class="inner-list row">
				<a class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<form action="/file-upload" class="dropzone" id="my-awesome-dropzone">
								<div class="add-item" for="browse-image">
									<input id="browse-image" type="file" class="hideit" multiple >
									<div>Drop file here or click to upload</div>
								</div>
							</form>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
				<a href="#" class="teamGallery-item">
					<div class="r-image">
						<div class="inner-image">
							<div class="bg-image" style="background-image:url(images/jpg/Img-Futsal04.jpg);"></div>
						</div>
					</div>
				</a>
			</div>

			<div class="oper-paging">
				<ul>
					<li><a href="">Prev</a></li>
					<li><a href="">1</a></li>
					<li><a href="" class="active">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">5</a></li>
					<li><a href="">Next</a></li>
				</ul>
			</div>
		</div>
<!-- End - Gallery All -->
	</div>
</div>


<!-- Begin - Text -->
<!-- End - Text -->

<!-- Begin - Text -->
<!-- End - Text -->

<!-- Begin - Text -->
<!-- End - Text -->

<!-- Begin - Text -->
<!-- End - Text -->

<!-- Begin - Popup Modals -->
<div id="modal-createAlbum" class="m-modals">
	<div class="background-wrap"></div>
	<div class="modal-wrap">
		<div class="tittle">Create a new album</div>
		<div class="body">
			<div class="desc md"></div>
			<div class="form-section">
				<label>Tittle album</label>
				<input type="text" placeholder="My Second Match">
			</div>
			<div class="form-section">
				<label>Destionation</label>
				<input type="text" placeholder="dimana kmu main?">
			</div>
			<div class="form-section">
				<label>Desctription</label>
				<textarea placeholder="Tell us your story"></textarea>
			</div>
		</div>
		<div class="foot row">
			<a href="" class="l-button blue">Create</a>
		</div>
	</div>
</div>
<!-- End - Popup Modals -->

<script src="js/views/team-detail.js"></script>