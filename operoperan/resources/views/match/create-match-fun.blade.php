@extends('layout.index')

@section('content')
<div class="container conY">
	<div class="m-tittle">Create Fun Match.</div>
	<div class="row">
<!-- Begin - Visual Wrap -->
		<div class="col-md-6">
			

			<div class="l-tittle blue"><span>Choose player</span></div>
			<div class="create-team row">
				<input type="text" id="inputPlayer" name="playerInput[]" value="" class="hideit">
				<div class="result-player no-margin">
					<!-- Defaultnya kosong -->
				</div>

				<div class="player-wrap">
					<div class="m-searchInput row">
						<input type="text" class="pull-left">
						<button class="pull-left"><i class="lnr lnr-magnifier"></i></button>
					</div>
				</div>

				<div class="text">
					Enjoy the quality time play futsal with your friend
				</div>

				<div class="result-search">
					<div class="player-wrap" data-name="aryatama">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Aryatama Krishna</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="fathur">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Fathureza Januarza</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="gabriel">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
							<div class="bio pull-left">
								<p>Gabriel Caesario</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="akbar">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
							<div class="bio pull-left">
								<p>Akbar Dwiyoga</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="syam">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
							<div class="bio pull-left">
								<p>Syam Sadane</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="parentcenter">
					<a href="#" data-target="#modal-invitePlayer" class="l-button blue modals-trigger">Send invitation</a>
				</div>
			</div>
		</div>	
<!-- End - Visual Wrap -->

<!-- Begin - Data Text Wrap -->
		<div class="col-md-6">
			<div class="form-section">
				<label>Nama Arena</label>
				<select class="chosen-select select" name="futsalArena">
					<option value="" class="hideit" selected>Choose futsal arena</option>
					<option value="1">Futsal Ceria</option>
					<option value="2">Kick89</option>
				</select>
				<div class="error-state">Belum di isi.</div>
			</div>
			<div class="form-section">
				<label>Kota</label>
				<input type="text" name="city">
				<div class="error-state">Belum di isi.</div>
			</div>
			<div class="form-section">
				<label>Alamat</label>
				<input type="text" name="address">
				<div class="error-state">Belum di isi.</div>
			</div>
			<div class="form-section row">
				<div class="half">
					<label>Tanggal</label>
					<input type="text" class="m-datepicker" name="date">
					<div class="error-state">Belum di isi.</div>
				</div>
				<div class="half">
					<label>Waktu</label>
					<select class="select" name="time">
						<option value="07:00 AM" selected>07:00 AM</option>
						<option value="07:00 PM">07:00 PM</option>
					</select>
					<div class="error-state">Belum di isi.</div>
				</div>
			</div>
			<div class="form-section">
				<label>Deskripsi</label>
				<textarea name="description"></textarea>
				<div class="error-state">Belum di isi.</div>
			</div>
			<div class="form-section">
				<label>Pembayaran</label>
				<div class="row">
					<div class="radio-wrap pull-left" data-name="payment-type">
						<div class="radio-box row">
							<span></span>
							<p>Free</p>
						</div>
						<input type="radio" value="Free" name="payment-type">
					</div>
					<div class="radio-wrap pull-left" data-name="payment-type">
						<div class="radio-box row">
							<span></span>
							<p>50:50</p>
						</div>
						<input type="radio" value="50:50" name="payment-type" checked>
					</div>
					<div class="radio-wrap pull-left" data-name="payment-type">
						<div class="radio-box row">
							<span></span>
							<p>The Loser</p>
						</div>
						<input type="radio" value="The Loser" name="payment-type">
					</div>
				</div>
				<div class="error-state">Belum di isi.</div>
			</div>
		</div>	
<!-- End - Data Text Wrap -->
	</div>

	<hr>

	<div class="parentright">
		<button class="l-button blue">Create</button>
	</div>
</div>

<!-- Begin - Popup Modals -->
<div id="modal-invitePlayer" class="m-modals">
	<div class="background-wrap"></div>
	<div class="modal-wrap">
		<div class="tittle">Send Invitation!</div>
		<div class="body">
			<input type="text" placeholder="email@gmail.com">
			<div class="desc sm">input email address of your candidates to invite them in your team. lets Play Futsal!</div>
		</div>
		<div class="foot">
			<a href="" class="m-button blue">Send</a>
		</div>
	</div>
</div>
<!-- End - Popup Modals -->

<script src="{{asset('js/views/create.js')}}"></script>

@endsection