@extends('layout.index')

@section('content')
<div class="container conY">
	<div class="m-tittle">Create Challange Match.</div>
	<div class="row">
<!-- Begin - Visual Wrap -->
		<div class="col-md-6">
			<!-- <div class="l-tittle blue"><span>Choose player</span></div> -->
			<div class="create-match row">
				<div class="team-wrap row">
					<div class="team-pic pull-left">
						<div class="r-image">
							<div class="inner-image">
								<img src="{{asset('image/jpg/'.$teamLogo)}}" alt="">
							</div>
						</div>
					</div>
					<div class="tittle pull-left">{!! $teamName !!}</div>
				</div>
				@foreach($players as $player)
				<div class="player-wrap">
					<div class="player-visit row">
						<div class="m-image xs pull-left"><img src="{{asset('image/jpg/'.$player->PLYR_PROFILE_IMG)}}" alt=""></div>
						<div class="bio pull-left">
							<p>{!! $player->PLYR_FIRST_NAME.' '.$player->PLYR_LAST_NAME !!}</p>
							<p>{!! $player->TEAM_NAME !!}</p>
						</div>
					</div>
				</div>
				@endforeach
				<div class="parentcenter">
					<a href="#" data-target="#modal-invitePlayer" class="l-button blue modals-trigger">Send invitation</a>
				</div>
			</div>
		</div>	
<!-- End - Visual Wrap -->

<!-- Begin - Data Text Wrap -->
		<div class="col-md-6">
			{!! Form::open(array('route' => 'match.challenge.create', 'method' => 'POST')) !!}
			<input type="hidden" name="teamID" value="{!! $teamID !!}" />
			<div class="form-section {{$errors->has('futsalArena') ? 'error' : ''}}">
				<label>Nama Arena</label>
				<select class="chosen-select select" name="futsalArena">
					<option value="" class="hideit" selected>Choose futsal arena</option>
					@foreach($arenas as $arena)
						<option value="{!! $arena->ARENA_ID !!}">{!! $arena->ARENA_NAME !!}</option>
					@endforeach
				</select>
				@if($errors->has('futsalArena'))
					<div class="error-state">{{$errors->first('futsalArena')}}</div>
				@endif
			</div>
			<div class="form-section {{$errors->has('city') ? 'error' : ''}}">
				<label>Kota</label>
				<input type="text" name="city" value="{{old('city')}}">
				@if($errors->has('city'))
					<div class="error-state">{{$errors->first('city')}}</div>
				@endif
			</div>
			<div class="form-section {{$errors->has('address') ? 'error' : ''}}">
				<label>Alamat</label>
				<input type="text" name="address" value="{{old('address')}}">
				@if($errors->has('address'))
					<div class="error-state">{{$errors->first('address')}}</div>
				@endif
			</div>
			<div class="form-section row {{$errors->has('date') ? 'error' : ''}}">
				<div class="half {{$errors->has('date') ? 'error' : ''}}">
					<label>Tanggal</label>
					<input type="text" class="m-datepicker" name="date" value="{{old('date')}}">
					@if($errors->has('date'))
						<div class="error-state">{{$errors->first('date')}}</div>
					@endif
				</div>
				<div class="half {{$errors->has('time') ? 'error' : ''}}">
					<label>Waktu</label>
					<select class="select" name="time">
						<option value="07:00 AM" selected>07:00 AM</option>
						<option value="07:00 PM">07:00 PM</option>
					</select>
					@if($errors->has('time'))
						<div class="error-state">{{$errors->first('time')}}</div>
					@endif
				</div>
			</div>
			<div class="form-section">
				<label>Deskripsi</label>
				<textarea name="description">{{old('description')}}</textarea>
				<div class="error-state">Belum di isi.</div>
			</div>
			<div class="form-section {{$errors->has('payment-type') ? 'error' : ''}}">
				<label>Pembayaran</label>
				<div class="row">
					<div class="radio-wrap pull-left" data-name="payment-type">
						<div class="radio-box row">
							<span></span>
							<p>Free</p>
						</div>
						<input type="radio" value="Free" name="payment-type">
					</div>
					<div class="radio-wrap pull-left active" data-name="payment-type">
						<div class="radio-box row">
							<span></span>
							<p>50:50</p>
						</div>
						<input type="radio" value="50:50" name="payment-type" checked>
					</div>
					<div class="radio-wrap pull-left" data-name="payment-type">
						<div class="radio-box row">
							<span></span>
							<p>The Loser</p>
						</div>
						<input type="radio" value="Loser" name="payment-type">
					</div>
				</div>
				@if($errors->has('payment-type'))
					<div class="error-state">{{$errors->first('payment-type')}}</div>
				@endif
			</div>


		</div>	
<!-- End - Data Text Wrap -->
	</div>

	<hr>

	<div class="parentright">
		<button class="l-button blue">Create</button>
	</div>
	{!! Form::close() !!}
</div>

<!-- Begin - Popup Modals -->

<div id="modal-invitePlayer" class="m-modals">
	<div class="background-wrap"></div>
	<div class="modal-wrap">
		<div class="tittle">Send Invitation!</div>
		<form action="#" method="post">
			<div class="body">
				{!! csrf_field() !!}
				<input type="hidden" name="playerID" value="{!! \Illuminate\Support\Facades\Auth::user()->PLYR_ID !!}" />
				<input type="text" name="email" placeholder="email@gmail.com">
				<div class="desc sm">input email address of your candidates to invite them in your team. lets Play Futsal!</div>
			</div>
			<div class="foot">
				<a href="#" class="m-button blue">Send</a>
			</div>
		</form>
	</div>
</div>
<!-- End - Popup Modals -->

<script src="{{asset('js/views/create.js')}}"></script>

@endsection