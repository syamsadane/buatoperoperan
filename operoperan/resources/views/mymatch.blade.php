@extends('layout.index')

@section('content')
<div class="mymatch-section">
	<div class="container">
		<div class="m-tittle head">My Match.</div>
		<div class="mymatch-wrap row">
			<div class="list-wrap pull-left">
				
				<!-- Begin - Tabs -->
				<div class="m-allTabs">
					<div class="tabs-wrap">
						<ul>
							<li><a href="#availableList" class="active green">Available</a></li>
							<li><a href="#upcomingList" class="blue">Upcoming</a></li>
							<li><a href="#resultList" class="yellow">Result</a></li>
						</ul>
					</div>
				</div>
				<!-- End - Tabs -->
				
				<!-- Begin - Available -->
				<div id="availableList" class="t-allTabs">
					<div class="available-list row">
						<div class="img-wrap pull-left">
							<img src="images/jpg/team-logo.jpg" alt="">
						</div>
						<div class="data-wrap pull-left">
							<div class="icon-text grey">
								<i class="lnr lnr-calendar-full"></i>
								<span>Wed, 10 Aug 2016. 07:00 PM</span>
							</div>
							<div class="icon-text grey">
								<i class="lnr lnr-location"></i>
								<span>Futsal Ceria, Jakarta Selatan</span>
							</div>
							<div class="icon-text grey">
								<i class="lnr lnr-diamond"></i>
								<span>50 : 50</span>
							</div>
						</div>
						<div class="button-wrap pull-right">
							<a href="" class="m-button red">Challange</a>
						</div>
					</div>
					<div class="available-list row">
						<div class="img-wrap pull-left">
							<img src="images/jpg/team-logo.jpg" alt="">
						</div>
						<div class="data-wrap pull-left">
							<div class="icon-text grey">
								<i class="lnr lnr-calendar-full"></i>
								<span>Wed, 10 Aug 2016. 07:00 PM</span>
							</div>
							<div class="icon-text grey">
								<i class="lnr lnr-location"></i>
								<span>Futsal Ceria, Jakarta Selatan</span>
							</div>
							<div class="icon-text grey">
								<i class="lnr lnr-diamond"></i>
								<span>50 : 50</span>
							</div>
						</div>
						<div class="button-wrap pull-right">
							<a href="" class="l-button red">Fun</a>
						</div>
					</div>

					<div class="oper-paging">
						<ul>
							<li><a href="">Prev</a></li>
							<li><a href="">1</a></li>
							<li><a href="" class="active">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">4</a></li>
							<li><a href="">5</a></li>
							<li><a href="">Next</a></li>
						</ul>
					</div>
				</div>
				<!-- End - Available -->
				
				<!-- Begin - Upcoming -->
				<div id="upcomingList" class="hideit t-allTabs">
					<div class="upcoming-list">
						<div class="top-wrap row">
							<div class="team-wrap left pull-left">
								<img src="images/jpg/team-logo.jpg" alt="">
								<a href="#">Shutetsu</a>
							</div>
							<span>VS</span>
							<div class="team-wrap right pull-right">
								<a href="#">Nankatsu</a>
								<img src="images/jpg/team-logo.jpg" alt="">
							</div>
						</div>
						<div class="bottom-wrap">
							<p>Wed, 10 Aug 2016. 07:00 PM</p>
							<p>Futsal Ceria, Jakarta Selatan</p>
						</div>
					</div>

					<div class="oper-paging">
						<ul>
							<li><a href="">Prev</a></li>
							<li><a href="">1</a></li>
							<li><a href="" class="active">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">4</a></li>
							<li><a href="">5</a></li>
							<li><a href="">Next</a></li>
						</ul>
					</div>
				</div>
				<!-- End - Upcoming -->
				
				<!-- Begin - Result -->
				<div id="resultList" class="hideit t-allTabs">
					<div class="upcoming-list">
						<div class="top-wrap row">
							<div class="team-wrap left pull-left">
								<img src="images/jpg/team-logo.jpg" alt="">
								<a href="#">Shutetsu</a>
							</div>
							<span>:</span>
							<div class="team-wrap right pull-right">
								<a href="#">Nankatsu</a>
								<img src="images/jpg/team-logo.jpg" alt="">
							</div>
						</div>
						<div class="bottom-wrap row">
							<div class="score-wrap pull-left">2</div>
							<div class="score-wrap pull-right">0</div>
						</div>
					</div>

					<div class="upcoming-list">
						<div class="top-wrap row">
							<div class="team-wrap left pull-left">
								<img src="images/jpg/team-logo.jpg" alt="">
								<a href="#">Shutetsu</a>
							</div>
							<span>:</span>
							<div class="team-wrap right pull-right">
								<a href="#">Nankatsu</a>
								<img src="images/jpg/team-logo.jpg" alt="">
							</div>
						</div>
						<div class="bottom-wrap row">
							<div class="score-wrap pull-left">2</div>
							<div class="score-wrap pull-right">0</div>
						</div>
					</div>

					<div class="oper-paging">
						<ul>
							<li><a href="">Prev</a></li>
							<li><a href="">1</a></li>
							<li><a href="" class="active">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">4</a></li>
							<li><a href="">5</a></li>
							<li><a href="">Next</a></li>
						</ul>
					</div>
				</div>
				<!-- End - Result -->
			</div>
			<div class="create-wrap pull-right">
				<div class="mymatch-create">
					<img src="images/png/bola1.png" alt="">
					<div class="tittle">Lorem ipsum dolor sit amet,<br> consectetur adipisicing elit.</div>
					<a href="#" class="l-button red">Create match</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection