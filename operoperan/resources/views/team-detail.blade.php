@extends('layout.index')
@include('layout.message.message')

@section('content')
<!-- Begin - Image Banner -->
<div class="team-banner">
	<div class="banner-image">
		<div class="inner-image">
			<img src="{{asset('image/jpg/Img-Futsal01.jpg')}}" alt="">
		</div>
	</div>
</div>
<!-- End - Image Banner -->

<!-- Begin - Bio Arena Detail -->
<div class="teamDetail-bio">
	<div class="container">
		<div class="row">
			<div class="inner-bio pull-left row">
				<div class="pic-wrap pull-left">
					<div class="r-image">
						<div class="inner-image">
							<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
						</div>
					</div>
				</div>
				<div class="bio-wrap pull-left">
					<div class="tittle">Mafia Migas FC</div>
					<div class="city">Jakarta Selatan</div>
					<div class="status">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi eveniet, cumque pariatur. Numquam, in dignissimos eaque provident eius ipsa cupiditate mollitia amet eveniet voluptate itaque! Adipisci, dolorum aperiam quas quod.</div>
				</div>
			</div>
			<div class="inner-bio pull-right row">
				<div class="ranking-wrap pull-right">
					<p>Ranking</p>
					<p>9217</p>
				</div>
				<div class="menu-wrap pull-right">
					<ul>
						<li><a href="#">Profile</a></li>
						<li><a href="#">Gallery</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End - Bio Arena Detail -->

<div class="teamDetail-section">
	<div class="container">
		<div class="row">
			<!-- Begin - Main Detail -->
			<div class="teamDetail-main">
				<table class="klasemen-table">
					<thead>
					<tr>
						<td>P</td>
						<td>W</td>
						<td>D</td>
						<td>L</td>
						<td>GF</td>
						<td>GA</td>
						<td>Pts.</td>
					</tr>
					</thead>
					<tbdoy>
						<tr>
							<td>16</td>
							<td>4</td>
							<td>5</td>
							<td>7</td>
							<td>12</td>
							<td>9</td>
							<td>87</td>
						</tr>
					</tbdoy>
				</table>

				<div class="team-nextMatch">
					<div class="type-wrap"><span>Challange</span></div>
					<div class="desc-wrap">
						<div class="icon-text grey">
							<i class="lnr lnr-calendar-full"></i>
							<span>Wed, 10 Aug 2016. 07:00 PM</span>
						</div>
						<div class="icon-text grey">
							<i class="lnr lnr-location"></i>
							<span>Futsal Ceria, Jakarta Selatan</span>
						</div>
						<div class="icon-text grey">
							<i class="lnr lnr-diamond"></i>
							<span>Free</span>
						</div>
					</div>
					<div class="versus-wrap">
						<span>VS</span>
						<div class="team-wrap">
							<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
							<span>Mafia Migas FC</span>
						</div>
					</div>
				</div>

				<div class="team-table row">
					<div class="col-md-6">
						<div class="l-tittle blue"><span>Upcoming</span></div>

						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Mafia Migas</a>
								</div>
								<span>VS</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap">
								<p>Wed, 10 Aug 2016. 07:00 PM</p>
								<p>Futsal Ceria, Jakarta Selatan</p>
							</div>
						</div>
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Mafia Migas</a>
								</div>
								<span>VS</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap">
								<p>Wed, 10 Aug 2016. 07:00 PM</p>
								<p>Futsal Ceria, Jakarta Selatan</p>
							</div>
						</div>
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Mafia Migas</a>
								</div>
								<span>VS</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap">
								<p>Wed, 10 Aug 2016. 07:00 PM</p>
								<p>Futsal Ceria, Jakarta Selatan</p>
							</div>
						</div>

						<a href="#" class="full-button">View All</a>
					</div>
					<div class="col-md-6">
						<div class="l-tittle yellow"><span>Result</span></div>

						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>:</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap row">
								<div class="score-wrap pull-left">2</div>
								<div class="score-wrap pull-right">0</div>
							</div>
						</div>
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>:</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap row">
								<div class="score-wrap pull-left">2</div>
								<div class="score-wrap pull-right">0</div>
							</div>
						</div>
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>:</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap row">
								<div class="score-wrap pull-left">2</div>
								<div class="score-wrap pull-right">0</div>
							</div>
						</div>

						<a href="#" class="full-button">View All</a>
					</div>
				</div>
			</div>
			<!-- End - Main Detail -->

			<div class="teamDetail-sub">
				<!-- Begin - Team Member -->
				<div class="teamDetail-member">
					<div class="l-tittle blue"><span>Members</span></div>

					<div class="player-wrap" data-name="aryatama">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Aryatama Krishna</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<!-- <a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a> -->
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="aryatama">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Aryatama Krishna</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="aryatama">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Aryatama Krishna</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="aryatama">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Aryatama Krishna</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
					<div class="player-wrap" data-name="aryatama">
						<div class="player-visit row">
							<div class="m-image xs pull-left"><img src="{{asset('image/jpg/profpic1.jpg')}}" alt=""></div>
							<div class="bio pull-left">
								<p>Aryatama Krishna</p>
								<p>Mafia Migas FC</p>
							</div>
							<div class="btn-wrap pull-left">
								<a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
							</div>
						</div>
					</div>
				</div>
				<!-- End - Team Member -->

				<!-- Begin - Team Achievements -->
				<div class="teamDetail-collection">
					<div class="l-tittle blue"><span>collections</span></div>
					<div class="row">
						<div class="collect-box pull-left">
							<div class="collect-item">
								<img src="{{asset('image/png/collect.png')}}" alt="">
								<p>Juara 1</p>
								<span>text</span>
							</div>
						</div>
						<div class="collect-box pull-left">
							<div class="collect-item">
								<img src="{{asset('image/png/collect.png')}}" alt="">
								<p>Juara 2</p>
								<span>text</span>
							</div>
						</div>
						<div class="collect-box pull-left">
							<div class="collect-item">
								<img src="{{asset('image/png/collect.png')}}" alt="">
								<p>Juara 3</p>
								<span>text</span>
							</div>
						</div>
						<div class="collect-box pull-left">
							<div class="collect-item">
								<img src="{{asset('image/png/collect.png')}}"alt="">
								<p>Juara Dunia</p>
								<span>text</span>
							</div>
						</div>
						<div class="collect-box pull-left">
							<div class="collect-item">
								<img src="{{asset('image/png/collect.png')}}" alt="">
								<p>Big leg</p>
								<span>(3/12)</span>
							</div>
						</div>
						<div class="collect-box pull-left">
							<div class="collect-item">
								<img src="{{asset('image/png/collect.png')}}" alt="">
								<p>Juara 1</p>
								<span>text</span>
							</div>
						</div>
					</div>
					<a href="#" class="full-button">View All</a>
				</div>
				<!-- End - Team Achievements -->
			</div>
		</div>
	</div>
</div>

<!-- Begin - Text -->
<!-- End - Text -->

<script src="{{asset('js/views/team-detail.js')}}"></script>
@endsection