@extends('layout.index')


@section('content')
    <div class="container conY">
        <div class="m-tittle">Create Team.</div>
        <div class="row">
            <!-- Begin - Visual Wrap -->
            {!! Form::open(array('route' => 'team.post', 'method' => 'POST' , 'files' => 'true')) !!}
            <div class="col-md-6">
                <div class="l-tittle blue"><span>Photo team</span></div>
                <div class=" create-team row">
                    <div class="img-wrap pull-left">
                        <div class="m-image sm"><i class="lnr lnr-picture"></i></div>
                    </div>

                    <div class="browse-wrap pull-left {{$errors->has('teamLogo') ? 'error' : '' }}">
                        <div class="image-browse no-margin">
                            <input type="file" id="file-img" name="teamLogo">
                            <label for="file-img" class="row">
                                <strong class="pull-right">
                                    <div class="icon-text">
                                        <i class="lnr lnr-download"></i>
                                        <span>Browse</span>
                                    </div>
                                </strong>

                                <p class="pull-right">&nbsp;</p>
                            </label>
                        </div>
                        <div class="parentleft">
                            <a href="#" class="l-button blue">Select photo</a>
                        </div>
                    </div>
                    @if($errors->has('teamLogo'))
                        <div class="error-state">{{ $errors->first('teamLogo') }}</div>
                    @endif
                </div>

                <div class="l-tittle blue"><span>Choose player</span></div>
                <div class="create-team row">
                    <input type="text" id="inputPlayer" name="playerInput[]" value="{!! \Illuminate\Support\Facades\Auth::user()->PLYR_ID !!}" class="hideit">
                    <div class="result-player">
                        <div class="player-wrap">
                            <div class="player-visit row">
                                <div class="m-image xs pull-left"> <img src="/image/jpg/{{Auth::user()->PLYR_PROFILE_IMG}}" alt=""></div>
                                <div class="bio pull-left">
                                    <p>{!! \Illuminate\Support\Facades\Auth::user()->PLYR_FIRST_NAME.' '.\Illuminate\Support\Facades\Auth::user()->PLYR_LAST_NAME !!}</p>

                                    {{--<p>Mafia Migas FC</p>--}}
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="player-wrap">
                        <div class="m-searchInput row">
                            <input type="text" class="pull-left">
                            <button class="pull-left"><i class="lnr lnr-magnifier"></i></button>
                        </div>
                    </div>

                    <div class="text">
                        Choose wisely 5 person for your own team
                    </div>

                    <div class="result-search">
                        @foreach($players as $player)
                            <div class="player-wrap" data-name="{!! $player->PLYR_ID !!}">
                                <div class="player-visit row">
                                    <div class="m-image xs pull-left"><img src="{!! asset(substr($player->PLYR_PROFILE_IMG, strpos($player->PLYR_PROFILE_IMG, 'image'), strlen($player->PLYR_PROFILE_IMG)))  !!}" alt=""></div>
                                    <div class="bio pull-left">
                                        <p>{!! $player->PLYR_FIRST_NAME.' '.$player->PLYR_LAST_NAME !!}</p>

                                        {{--<p>Mafia Migas FC</p>--}}
                                    </div>
                                    <div class="btn-wrap pull-left">
                                        <a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
                                        <a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{--<div class="player-wrap" data-name="gabriel">
                            <div class="player-visit row">
                                <div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
                                <div class="bio pull-left">
                                    <p>Gabriel Caesario</p>

                                    <p>Mafia Migas FC</p>
                                </div>
                                <div class="btn-wrap pull-left">
                                    <a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
                                    <a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="player-wrap" data-name="akbar">
                            <div class="player-visit row">
                                <div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
                                <div class="bio pull-left">
                                    <p>Akbar Dwiyoga</p>

                                    <p>Mafia Migas FC</p>
                                </div>
                                <div class="btn-wrap pull-left">
                                    <a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
                                    <a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="player-wrap" data-name="syam">
                            <div class="player-visit row">
                                <div class="m-image xs pull-left"><i class="lnr lnr-picture"></i></div>
                                <div class="bio pull-left">
                                    <p>Syam Sadane</p>

                                    <p>Mafia Migas FC</p>
                                </div>
                                <div class="btn-wrap pull-left">
                                    <a href="#" class="icon add-player"><i class="lnr lnr-plus-circle"></i></a>
                                    <a href="#" class="icon remove-player"><i class="lnr lnr-cross"></i></a>
                                </div>
                            </div>
                        </div>--}}
                    </div>

                    {{$players->links()}}

                    <div class="parentcenter">
                        <a href="#" data-target="#modal-invitePlayer" class="l-button blue modals-trigger">Send
                            invitation</a>
                    </div>
                </div>
            </div>
            <!-- End - Visual Wrap -->

            <!-- Begin - Data Text Wrap -->
            <div class="col-md-6">
                <div class="form-section {{ $errors->has('teamName') ? 'error' : '' }}">
                    <label>Nama team</label>
                    <input type="text" name="teamName" value="{{ old('teamName') }}">
                    @if($errors->has('teamName'))
                        <div class="error-state">{{ $errors->first('teamName') }}</div>
                    @endif
                </div>
                <div class="form-section {{ $errors->has('teamCity') ? 'error' : ''}}">
                    <label>Kota</label>
                    <select class="select" name="teamCity">
                        <option> --- Select Your City ---</option>
                        <option value="Jakarta">Jakarta</option>
                        <option value="Bandung">Bandung</option>
                        <option value="Bali">Bali</option>
                    </select>
                    @if($errors->has('teamCity'))
                        <div class="error-state">{{ $errors->first('teamCity') }}</div>
                    @endif
                </div>
                <div class="form-section" {{ $errors->has('gender') ? 'error' : ''}}>
                    <label>Gender</label>

                    <div class="row">
                        <div class="radio-wrap pull-left" data-name="gender">
                            <div class="radio-box row">
                                <span></span>

                                <p>Man</p>
                            </div>
                            <input type="radio" name="gender">
                        </div>
                        <div class="radio-wrap pull-left" data-name="gender">
                            <div class="radio-box row">
                                <span></span>

                                <p>Female</p>
                            </div>
                            <input type="radio" name="gender">
                        </div>
                    </div>
                    @if($errors->has('gender'))
                        <span class="help-block">{{ $errors->first('gender') }}</span>
                    @endif
                </div>
                <div class="form-section {{ $errors->has('teamDescription') ? 'error' : '' }}">
                    <label>Deskripsi</label>
                    <textarea name="teamDescription">{{ old('teamDescription') }}</textarea>
                    @if($errors->has('teamDescription'))
                        <div class="error-state">{{ $errors->first('teamDescription') }}</div>
                    @endif
                </div>
            </div>
            <!-- End - Data Text Wrap -->
        </div>

        <hr>

        <div class="parentright">
            <button class="l-button blue">Create</button>
        </div>

        {!! Form::close() !!}
    </div>

    <!-- Begin - Popup Modals -->
    <div id="modal-invitePlayer" class="m-modals">
        <div class="background-wrap"></div>
        <div class="modal-wrap">
            <div class="tittle">Send Invitation!</div>
            <div class="body">
                <input type="text" placeholder="email@gmail.com">

                <div class="desc sm">input email address of your candidates to invite them in your team. lets Play
                    Futsal!
                </div>
            </div>
            <div class="foot">
                <a href="" class="m-button blue">Send</a>
            </div>
        </div>
    </div>
    <!-- End - Popup Modals -->

    <script src="{{asset('js/views/create.js')}}"></script>
@endsection