@extends('layout.index')


@section('content')



    @include('layout.message.message')
    <div class="background-color grey">
        <div class="container">
            <!-- Begin - Player Information -->
            <div class="profile-player row">
                <div class="img-player pull-left">
                    <img src="/image/jpg/{{Auth::user()->PLYR_PROFILE_IMG }}" alt="">
                </div>
                <div class="detail-player pull-left">
                    <div class="l-tittle green"><span>Biodata</span></div>
                    <div class="detail-wrap row">
                        <div class="col-md-5">
                            <div class="bio-wrap row">
                                <p class="pull-left">Nama</p>

                                <p class="pull-left">{{ $player->PLYR_FIRST_NAME }}</p>
                            </div>
                            <div class="bio-wrap row">
                                <p class="pull-left">Gender</p>

                                <p class="pull-left">{{ $player->PLYR_GENDER }}</p>
                            </div>
                            <div class="bio-wrap row">
                                <p class="pull-left">Birth Date</p>

                                <p class="pull-left">{{ $player->PLYR_DOB }}</p>
                            </div>
                            <div class="bio-wrap row">
                                <p class="pull-left">City</p>

                                <p class="pull-left">{{ $player->PLYR_CITY }}</p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="bio-wrap row">
                                <p class="pull-left">Full Name</p>

                                <p class="pull-left">{{$player->PLYR_FIRST_NAME}} {{$player->PLYR_LAST_NAME}}</p>
                            </div>
                            <div class="bio-wrap row">
                                <p class="pull-left">Favourite No </p>

                                <p class="pull-left">{{$player->PLYR_FAVOURITE_NO}}</p>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="parentright">
                                <a href="" class="l-button blue">My match</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End - Player Information -->

            <div class="team-player">
                @if(Auth::User()->PLYR_FLAG_TEAM == 0)
                    <div class="l-tittle yellow"><span>Team</span></div>
                    <div class="empty row">
                        <p>You dont have team, you must get your team to start a match</p>

                        <p>
                            <a href="{{route('search')}}" class="l-button blue">Find a Team</a>
                            <a href="{{ route('team.create') }}" class="m-button blue">Create Team</a>
                        </p>
                    </div>
                @else


                    <div class="team-fill row ">
                            <div class="img-wrap pull-left">
                                <div class="image-null sm"></div>
                                <a href="" class="m-button red">Visit</a>
                            </div>
                            <div class="bio-wrap pull-left">
                                <div class="tittle"></div>
                                <div class="city">Jakarta Selatan</div>
                                <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam
                                    fugiat quos obcaecati harum, consequatur laborum tempora itaque quod dignissimos
                                    quas eligendi, illo excepturi, pariatur cumque veniam beatae et dolorum
                                    voluptatem.
                                </div>
                                <div class="desc">Anggota : 20 Orang</div>
                            </div>
                            <div class="team-wrap pull-right custom-scroll">
                                <div class="player-visit row">
                                    <div class="image-null xs pull-left"></div>
                                    <div class="bio pull-left">
                                        <p>Akbar Dwiyoga</p>

                                        <p>Mafia Migas FC</p>
                                    </div>
                                </div>
                                <div class="player-visit row">
                                    <div class="image-null xs pull-left"></div>
                                    <div class="bio pull-left">
                                        <p>Gerry Caesario</p>

                                        <p>Mafia Migas FC</p>
                                    </div>
                                </div>
                                <div class="player-visit row">
                                    <div class="image-null xs pull-left"></div>
                                    <div class="bio pull-left">
                                        <p>Aryatama Krishna</p>

                                        <p>Mafia Migas FC</p>
                                    </div>
                                </div>
                                <div class="player-visit row">
                                    <div class="image-null xs pull-left"></div>
                                    <div class="bio pull-left">
                                        <p>Fathureza Januarza</p>

                                        <p>Mafia Migas FC</p>
                                    </div>
                                </div>
                                <div class="player-visit row">
                                    <div class="image-null xs pull-left"></div>
                                    <div class="bio pull-left">
                                        <p>Syam Sadane</p>

                                        <p>Mafia Migas FC</p>
                                    </div>
                                </div>
                            </div>
                    </div>
            </div>
            @endif
        </div>
    </div>

@endsection