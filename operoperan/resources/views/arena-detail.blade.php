<!-- Begin - Image Banner -->
<div class="arena-banner">
	<div class="banner-image">
		<div class="inner-image">
			<img src="images/jpg/Img-Futsal01.jpg" alt="">
		</div>
	</div>
	<div class="banner-image">
		<div class="inner-image">
			<img src="images/jpg/Img-Futsal02.jpg" alt="">
		</div>
	</div>
</div>
<!-- End - Image Banner -->

<!-- Begin - Bio Arena Detail -->
<div class="arena-bio">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="tittle">Futsal Kura-Kura Ninja</div>
				<div class="icon-text">
					<i class="lnr lnr-home"></i>
					<span>Komplek Billbong, Bojong Gede, Bogor.</span>
				</div>
				<div class="icon-text">
					<i class="lnr lnr-phone-handset"></i>
					<span>+6221 7646190</span>
				</div>
			</div>
			<div class="col-md-4 right-wrap">
				<div class="price">Rp. 150.000</div>
				<a href="#" class="l-button green">View Map</a>
			</div>
		</div>
	</div>
</div>
<!-- End - Bio Arena Detail -->

<!-- Begin - Arena Detail  -->
<div class="arenaDetail-wrap">
	<div class="container">
		<div class="row">
			<div class="tittle-wrap"><p>Description</p></div>
			<div class="desc-wrap"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, quidem. Et sunt quisquam saepe, id itaque, quod eligendi illum. Magni eaque ipsa ratione sunt, aliquid iure porro. Quae, molestiae, corrupti. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt repudiandae nam minima architecto temporibus illum, laboriosam, obcaecati dignissimos. Voluptate reprehenderit accusantium voluptates rem facilis dolore natus totam alias ab temporibus.</p></div>
		</div>
		<hr>
		<div class="row">
			<div class="tittle-wrap"><p>Amenities</p></div>
			<div class="desc-wrap row">
				<div class="icon-text pull-left">
					<i class="lnr lnr-phone-handset"></i>
					<span>AC</span>
				</div>
				<div class="icon-text pull-left">
					<i class="lnr lnr-phone-handset"></i>
					<span>2 Arena</span>
				</div>
				<div class="icon-text pull-left">
					<i class="lnr lnr-phone-handset"></i>
					<span>Rumput Arena</span>
				</div>
				<div class="icon-text pull-left">
					<i class="lnr lnr-phone-handset"></i>
					<span>Outdoor</span>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="tittle-wrap"><p>Oprational Hour</p></div>
			<div class="desc-wrap">
				<p>12:00 AM - 22:00 PM (Weekdays)</p>
				<p>24 Hours (Weekend)</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="tittle-wrap"><p>Price</p></div>
			<div class="desc-wrap">
				<!-- Begin - Arena Price -->
				<div class="arena-price">
					<div class="type-wrap row">
						<div class="img-wrap pull-left">
							<div class="inner-img">
								<img src="images/jpg/arena1.jpg" alt="">
							</div>
						</div>
						<div class="detail-wrap pull-left">
							<div class="detail-box">
								<div class="tittle">Jenis Lapangan</div>
								<div class="text">Rumput</div>
							</div>
							<div class="detail-box">
								<div class="tittle">Ukuran</div>
								<div class="text">100cm X  200cm</div>
							</div>
							<div class="detail-box">
								<div class="tittle">Jumlah</div>
								<div class="text">2</div>
							</div>
						</div>
					</div>
					<div class="table-wrap">
						<table width="100%" class="arena-table">
							<thead>
								<tr>
									<td>Hari</td>
									<td>Jam</td>
									<td>Harga</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Senin</td>
									<td>16:00 - 23:00</td>
									<td>Rp. 250.000</td>
								</tr>
								<tr>
									<td>Selasa</td>
									<td>00:00 - 15:00</td>
									<td>Rp. 150.000</td>
								</tr>
								<tr>
									<td>Rabu</td>
									<td>16:00 - 23:00</td>
									<td>Rp. 100.000</td>
								</tr>
								<tr>
									<td></td>
									<td>00:00 - 15:00</td>
									<td>Rp. 40.000</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="arena-price">
					<div class="type-wrap row">
						<div class="img-wrap pull-left">
							<div class="inner-img">
								<img src="images/jpg/arena2.jpg" alt="">
							</div>
						</div>
						<div class="detail-wrap pull-left">
							<div class="detail-box">
								<div class="tittle">Jenis Lapangan</div>
								<div class="text">Aspal</div>
							</div>
							<div class="detail-box">
								<div class="tittle">Ukuran</div>
								<div class="text">100cm X  200cm</div>
							</div>
							<div class="detail-box">
								<div class="tittle">Jumlah</div>
								<div class="text">1</div>
							</div>
						</div>
					</div>
					<div class="table-wrap">
						<table width="100%" class="arena-table">
							<thead>
								<tr>
									<td>Hari</td>
									<td>Jam</td>
									<td>Harga</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Selasa - Minggu</td>
									<td>16:00 - 23:00</td>
									<td>Rp. 250.000</td>
								</tr>
								<tr>
									<td></td>
									<td>00:00 - 15:00</td>
									<td>Rp. 150.000</td>
								</tr>
								<tr>
									<td>Senin</td>
									<td>16:00 - 23:00</td>
									<td>Rp. 100.000</td>
								</tr>
								<tr>
									<td></td>
									<td>00:00 - 15:00</td>
									<td>Rp. 40.000</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- End - Arena Price -->
			</div>
		</div>
	</div>
</div>
<!-- End - Arena Detail  -->

<!-- Begin - Google Maps -->
<div id="arenaMap" class="arena-map"></div>
<!-- End - Google Maps -->

<!-- Begin - Related Arena -->
<div class="container">
	<div class="arena-related row">
		<div class="m-tittle">Related Arena.</div>
		<a href="#" class="related-wrap col-md-3">
			<div class="r-image">
				<div class="inner-image">
					<div class="bg-image" style="background-image:url(images/jpg/arena1.jpg);"></div>
				</div>
			</div>
			<div class="tittle">Futsal Pura-pura</div>
		</a>

		<a href="#" class="related-wrap col-md-3">
			<div class="r-image">
				<div class="inner-image">
					<div class="bg-image" style="background-image:url(images/jpg/arena1.jpg);"></div>
				</div>
			</div>
			<div class="tittle">Rampage</div>
		</a>

		<a href="#" class="related-wrap col-md-3">
			<div class="r-image">
				<div class="inner-image">
					<div class="bg-image" style="background-image:url(images/jpg/arena1.jpg);"></div>
				</div>
			</div>
			<div class="tittle">Futsal Pura-pura</div>
		</a>

		<a href="#" class="related-wrap col-md-3">
			<div class="r-image">
				<div class="inner-image">
					<div class="bg-image" style="background-image:url(images/jpg/arena1.jpg);"></div>
				</div>
			</div>
			<div class="tittle">Futsal Pura-pura</div>
		</a>
	</div>
</div>
<!-- End - Related Arena -->

<script src="js/views/arena.js"></script>
<script>
	functions.push(function(){
		// function myMap(){
		var mapCanvas = document.getElementById("arenaMap");
		var mapOptions = {
			center: new google.maps.LatLng(51.5, -0.2), 
			zoom: 10,
			scrollwheel: false,
			navigationControl: false,
			mapTypeControl: false,
			scaleControl: false,
		}
		var map = new google.maps.Map(mapCanvas, mapOptions);

		map.set('styles', [
	    {
	        "featureType": "water",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#e9e9e9"
	            },
	            {
	                "lightness": 17
	            }
	        ]
	    },
	    {
	        "featureType": "landscape",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#f5f5f5"
	            },
	            {
	                "lightness": 20
	            }
	        ]
	    },
	    {
	        "featureType": "road.highway",
	        "elementType": "geometry.fill",
	        "stylers": [
	            {
	                "color": "#ffffff"
	            },
	            {
	                "lightness": 17
	            }
	        ]
	    },
	    {
	        "featureType": "road.highway",
	        "elementType": "geometry.stroke",
	        "stylers": [
	            {
	                "color": "#ffffff"
	            },
	            {
	                "lightness": 29
	            },
	            {
	                "weight": 0.2
	            }
	        ]
	    },
	    {
	        "featureType": "road.arterial",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#ffffff"
	            },
	            {
	                "lightness": 18
	            }
	        ]
	    },
	    {
	        "featureType": "road.local",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#ffffff"
	            },
	            {
	                "lightness": 16
	            }
	        ]
	    },
	    {
	        "featureType": "poi",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#f5f5f5"
	            },
	            {
	                "lightness": 21
	            }
	        ]
	    },
	    {
	        "featureType": "poi.park",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#dedede"
	            },
	            {
	                "lightness": 21
	            }
	        ]
	    },
	    {
	        "elementType": "labels.text.stroke",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "color": "#ffffff"
	            },
	            {
	                "lightness": 16
	            }
	        ]
	    },
	    {
	        "elementType": "labels.text.fill",
	        "stylers": [
	            {
	                "saturation": 36
	            },
	            {
	                "color": "#333333"
	            },
	            {
	                "lightness": 40
	            }
	        ]
	    },
	    {
	        "elementType": "labels.icon",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "transit",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "color": "#f2f2f2"
	            },
	            {
	                "lightness": 19
	            }
	        ]
	    },
	    {
	        "featureType": "administrative",
	        "elementType": "geometry.fill",
	        "stylers": [
	            {
	                "color": "#fefefe"
	            },
	            {
	                "lightness": 20
	            }
	        ]
	    },
	    {
	        "featureType": "administrative",
	        "elementType": "geometry.stroke",
	        "stylers": [
	            {
	                "color": "#fefefe"
	            },
	            {
	                "lightness": 17
	            },
	            {
	                "weight": 1.2
	            }
	        ]
	    }
	]);
		// }
	});
</script>