@extends('layout.index')

@section('content')
<div class="klasemen-section custom-dataTable">
	<div class="container">
		<div class="m-tittle head">Klasemen.</div>
		<table class="klasemen-table">
			<thead>
				<tr>
					<td>No.</td>
					<td>Nama team</td>
					<td>P</td>
					<td>W</td>
					<td>D</td>
					<td>L</td>
					<td>GF</td>
					<td>GA</td>
					<td>Pts.</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
				<tr>
					<td>1</td>
					<td>
						<img src="images/jpg/team-logo.jpg" alt="">
						<span>Mafia Migas FC</span>
					</td>
					<td>16</td>
					<td>4</td>
					<td>5</td>
					<td>7</td>
					<td>12</td>
					<td>9</td>
					<td>87</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script>
	functions.push(function(){
		$(".klasemen-table").DataTable({
			dom: 'frtp',
		});
	});
</script>

@endsection