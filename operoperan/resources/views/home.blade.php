@extends('layout.index')



@section('content')


@include('layout.message.message')
<!-- Begin - Hero Banner -->
@if(\Illuminate\Support\Facades\Auth::check())
	<input type="hidden" id="plyrFlagTeam" value="{!! \Illuminate\Support\Facades\Auth::user()->PLYR_FLAG_TEAM !!}"/>
@endif
<input type="hidden" id="plyrFlagLoggedIn" value="{!! \Illuminate\Support\Facades\Auth::check() !!}"/>
<div class="hero-wrap">
	<div class="hero-image" style="background-image:url({{asset('image/jpg/Img-Futsal01.jpg)')}});">
		<div class="inner">
			<h1>Play With Love</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
			<div class="row">
				<a href="{{route('match.challenge.create')}}" id="createMatchBtn" class="l-button black">Buat Match</a>
				<a href="{{route('team.create')}}" class="l-button black">Buat Team</a>
			</div>
		</div>
	</div>

	<div class="hero-image" style="background-image:url(https://images.unsplash.com/photo-1466065665758-d473db752253?dpr=1&auto=format&crop=entropy&fit=crop&w=1500&h=1125&q=80&cs=tinysrgb);">
		<div class="inner">
			<h1>Play With Love</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
			<div class="row">
				<a href="{{route('match.challenge.create')}}" class="l-button black">Buat Match</a>
				<a href="" class="l-button black">Buat Team</a>
			</div>
		</div>
	</div>

	<div class="hero-image" style="background-image:url({{asset('image/jpg/Img-Futsal03.jpg')}});">
		<div class="inner">
			<h1>Play With Love</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
			<div class="row">
				<a href="{{route('match.challenge.create')}}" class="l-button black">Buat Match</a>
				<a href="" class="l-button black">Buat Team</a>
			</div>
		</div>
	</div>
</div>
<!-- End - Hero Banner -->

<!-- Begin - Main Tabel  -->
<div class="background-color lightGrey">
	<div class="container">
		<div class="home-klasemen-wrap row">
			<div class="col-md-8">
				<div class="available-wrap">
					<div class="l-tittle green"><span>Available Match</span></div>

					<div>
						@foreach($matches as $match)
						<div class="available-list row">
							<div class="img-wrap pull-left">
								<img src="{{asset('image/jpg/'.$match->TEAM_LOGO)}}" alt="">
							</div>
							<div class="data-wrap pull-left">
								<div class="icon-text grey">
									<i class="lnr lnr-calendar-full"></i>
									<span>{!! $match->MATCH_DATE.' . '.$match->MATCH_TIME !!}</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-location"></i>
									<span>{!! $match->ARENA_NAME.', '.$match->ARENA_CITY !!}</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-diamond"></i>
									<span>{!! $match->MATCH_FEE_TYPE !!}</span>
								</div>
							</div>
							<div class="button-wrap pull-right">
								@if($match->MATCH_TYPE != null || $match->MATCH_TYPE != '' || strlen($match->MATCH_TYPE) > 0)
									@if (substr($match->MATCH_TYPE, 0, 1) === 'C')
										<a href="" class="m-button red">Challange</a>
									@else
										<a href="" class="l-button red">Fun</a>
									@endif
								@endif
							</div>
						</div>
						@endforeach

						{{--<div class="available-list row">
							<div class="img-wrap pull-left">
								<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
							</div>
							<div class="data-wrap pull-left">
								<div class="icon-text grey">
									<i class="lnr lnr-calendar-full"></i>
									<span>Wed, 10 Aug 2016. 07:00 PM</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-location"></i>
									<span>Futsal Ceria, Jakarta Selatan</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-diamond"></i>
									<span>The Loser</span>
								</div>
							</div>
							<div class="button-wrap pull-right">
								<a href="" class="m-button red">Challange</a>
							</div>
						</div>

						<div class="available-list row">
							<div class="img-wrap pull-left">
								<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
							</div>
							<div class="data-wrap pull-left">
								<div class="icon-text grey">
									<i class="lnr lnr-calendar-full"></i>
									<span>Wed, 10 Aug 2016. 07:00 PM</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-location"></i>
									<span>Futsal Ceria, Jakarta Selatan</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-diamond"></i>
									<span>Free</span>
								</div>
							</div>
							<div class="button-wrap pull-right">
								<a href="" class="l-button red">Fun</a>
							</div>
						</div>

						<div class="available-list row">
							<div class="img-wrap pull-left">
								<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
							</div>
							<div class="data-wrap pull-left">
								<div class="icon-text grey">
									<i class="lnr lnr-calendar-full"></i>
									<span>Wed, 10 Aug 2016. 07:00 PM</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-location"></i>
									<span>Futsal Ceria, Jakarta Selatan</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-diamond"></i>
									<span>50 : 50</span>
								</div>
							</div>
							<div class="button-wrap pull-right">
								<a href="" class="l-button red">Fun</a>
							</div>
						</div>

						<div class="available-list row">
							<div class="img-wrap pull-left">
								<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
							</div>
							<div class="data-wrap pull-left">
								<div class="icon-text grey">
									<i class="lnr lnr-calendar-full"></i>
									<span>Wed, 10 Aug 2016. 07:00 PM</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-location"></i>
									<span>Futsal Ceria, Jakarta Selatan</span>
								</div>
								<div class="icon-text grey">
									<i class="lnr lnr-diamond"></i>
									<span>50 : 50</span>
								</div>
							</div>
							<div class="button-wrap pull-right">
								<a href="" class="m-button red">Challange</a>
							</div>
						</div>--}}
					</div>
				</div>

				<div class="row home-tabel-match">
					<div class="dual-container">
						<div class="l-tittle blue"><span>Upcoming Match</span></div>
						
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Mafia Migas FC</a>
								</div>
								<span>VS</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap">
								<p>Wed, 10 Aug 2016. 07:00 PM</p>
								<p>Futsal Ceria, Jakarta Selatan</p>
							</div>
						</div>
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>VS</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap">
								<p>Wed, 10 Aug 2016. 07:00 PM</p>
								<p>Futsal Ceria, Jakarta Selatan</p>
							</div>
						</div>
						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>VS</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap">
								<p>Wed, 10 Aug 2016. 07:00 PM</p>
								<p>Futsal Ceria, Jakarta Selatan</p>
							</div>
						</div>
						
						<a href="" class="full-button">View All</a>
					</div>
					<div class="dual-container">
						<div class="l-tittle yellow"><span>Result Match</span></div>

						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>:</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap row">
								<div class="score-wrap pull-left">2</div>
								<div class="score-wrap pull-right">0</div>
							</div>
						</div>

						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>:</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap row">
								<div class="score-wrap pull-left">2</div>
								<div class="score-wrap pull-right">0</div>
							</div>
						</div>

						<div class="upcoming-list">
							<div class="top-wrap row">
								<div class="team-wrap left pull-left">
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
									<a href="#">Shutetsu</a>
								</div>
								<span>:</span>
								<div class="team-wrap right pull-right">
									<a href="#">Nankatsu</a>
									<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
								</div>
							</div>
							<div class="bottom-wrap row">
								<div class="score-wrap pull-left">2</div>
								<div class="score-wrap pull-right">0</div>
							</div>
						</div>

						<a href="" class="full-button">View All</a>
					</div>
				</div>
					
			</div>
			<div class="col-md-4">
				<div class="klasemen-wrap">
					<div class="tabs-wrap">
						<ul>
							<li><a href="#klasemenMan" class="active grey">Pria</a></li>
							<li><a href="#klasemenWoman" class="pink">Wanita</a></li>
						</ul>
					</div>
					<div id="klasemenMan" class="klasemen-gender">
						<table class="klasemen-table gender-man">
							<thead>
								<tr>
									<td>No.</td>
									<td>Nama Team</td>
									<td>P</td>
									<td>Pts.</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>2</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>3</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>4</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>5</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>6</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>7</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>8</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>9</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>10</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>11</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>12</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>13</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>14</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>15</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Mafia Migas FC</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="klasemenWoman" class="klasemen-gender hideit">
						<table class="klasemen-table gender-woman">
							<thead>
								<tr>
									<td>No.</td>
									<td>Nama Team</td>
									<td>P</td>
									<td>Pts.</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>2</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>3</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>4</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>5</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>6</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>7</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>8</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>9</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
								<tr>
									<td>10</td>
									<td>
										<img src="{{asset('image/jpg/team-logo.jpg')}}" alt="">
										<span>Sepak Lucu</span>
									</td>
									<td>21</td>
									<td>147</td>
								</tr>
							</tbody>
						</table>
					</div>
					<a href="#" class="full-button">View all</a>
				</div>
			</div>
		</div>
<!-- End - Main Tabel  -->

<!-- Begin - Banner Ads  -->
		<div class="banner-ads">
			<div class="banner-image">
				<div class="inner">
					<img src="{{asset('image/jpg/Img-Futsal01.jpg')}}" alt="">
				</div>
			</div>
		</div>
<!-- End - Banner Ads  -->
	</div>
</div>

<div class="container">
	<div class="home-news">
		<div class="m-tittle">News.</div>
		
		<div class="row">
			<div class="news-box quad-container">
				<div class="news-wrap">
					<div class="inner">
						<div class="back-img" style="background-image:url({{asset('image/jpg/Img-Futsal02.jpg')}});"></div>
					</div>
				</div>

				<div class="news-read">
					<div class="tags red">Hot</div>
					<div class="tittle">Main futsal sambil main Pokemon Go</div>
				</div>
			</div>

			<div class="news-box quad-container">
				<div class="news-wrap">
					<div class="inner">
						<div class="back-img" style="background-image:url({{asset('image/jpg/Img-Futsal02.jpg')}});"></div>
					</div>
				</div>

				<div class="news-read">
					<div class="tags blue">Skill</div>
					<div class="tittle">Main futsal sambil main Pokemon Go</div>
				</div>
			</div>

			<div class="news-box quad-container">
				<div class="news-wrap">
					<div class="inner">
						<div class="back-img" style="background-image:url({{asset('image/jpg/Img-Futsal03.jpg')}});"></div>
					</div>
				</div>

				<div class="news-read">
					<div class="tags yellow">Information</div>
					<div class="tittle">Main futsal sambil main Pokemon Go</div>
				</div>
			</div>

			<div class="news-box quad-container">
				<div class="news-wrap">
					<div class="inner">
						<div class="back-img" style="background-image:url({{asset('image/jpg/Img-Futsal03.jpg')}});"></div>
					</div>
				</div>

				<div class="news-read">
					<div class="tags green">Health</div>
					<div class="tittle">Main futsal sambil main Pokemon Go</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Begin - Popup Modals -->
<div id="modal-match" class="m-modals">
	<div class="background-wrap"></div>
	<div class="modal-wrap">
		<div class="tittle">What Type Match?</div>
		<div class="body">
			<!-- <input type="text" placeholder="email@gmail.com"> -->
			<div class="desc md">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, temporibus, optio. Ducimus accusamus animi distinctio expedita, vel delectus quos nostrum vero praesentium commodi maiores. Sunt illum aliquid porro nostrum repellat.</div>
		</div>
		<div class="foot row">
			<div class="half">
				<a href="{{ route('match.fun.create') }}" class="l-button red full">Fun</a>
			</div>
			<div class="half">
				<a href="{{route('match.challenge.create')}}" class="m-button red full">Challange</a>
			</div>
		</div>
	</div>
</div>
<!-- End - Popup Modals -->

<!-- Begin - Popup Modals Success -->
<div id="modal-successBox" class="m-modals">
	<div class="background-wrap"></div>
	<div class="modal-wrap">
		<!-- <div class="tittle">Error!</div> -->
		<div class="body">
			<div class="single-icon success">
				<i class="lnr lnr-smile"></i>
			</div>
			<div class="desc lg parentcenter">Success!</div>
			<div class="desc sm parentcenter">Your order has been update</div>
		</div>
		<div class="foot">
			<a href="" class="m-button green full">OK</a>
		</div>
	</div>
</div>
<!-- End - Popup Modals Success -->

<!-- Begin - Popup Modals Failed -->
<div id="modal-failedBox" class="m-modals">
	<div class="background-wrap"></div>
	<div class="modal-wrap">
		<!-- <div class="tittle">Error!</div> -->
		<div class="body">
			<div class="single-icon failed">
				<i class="lnr lnr-sad"></i>
			</div>
			<div class="desc lg parentcenter">Oops!</div>
			<div class="desc sm parentcenter">Sorry we have some issue</div>
		</div>
		<div class="foot">
			<a href="#" id="failedOkBtn" class="m-button red full">OK</a>
		</div>
	</div>
</div>
<!-- End - Popup Modals Failed -->

<script src="{{asset('js/views/home.js')}}"></script>

@endsection

