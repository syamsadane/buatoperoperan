functions.push(function(){
// This is for change img tag into svg 
// Begin
$('img.svg-image').each(function(){
    var $img = $(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a'); 

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');
});
// End

// This is for header sticky 
// Begin
$(window).scroll(function(){
    var scroll = $(this).scrollTop();

    if(scroll > 1){
        // index
        $("#header").addClass('sticky');
        $("#header").css('transition', '.5s');
    }else{
        // index
        $("#header").removeClass('sticky');
        $("#header").css('transition', '.4s');
    }
});
// End

// This is for All Tabs Menu
// Begin
$(".m-allTabs .tabs-wrap a").on("click", function(e){
	e.preventDefault();
	var href = $(this).attr("href");

	$(".m-allTabs .tabs-wrap a").removeClass("active");
	$(this).addClass("active");

	$(".t-allTabs").addClass("hideit");
	$(href).removeClass("hideit");
});
// End

// This is for Form Input Radio Button
// Begin
$(".form-section .radio-wrap .radio-box").on("click", function(){
	var name = $(this).parent().attr("data-name");

	$(".form-section .radio-wrap .radio-box").parents(".radio-wrap[data-name='"+name+"']").removeClass("active");
	$(this).parents(".radio-wrap[data-name='"+name+"']").addClass("active");

	$(".form-section .radio-wrap .radio-box").parents(".radio-wrap[data-name='"+name+"']").find("input").prop("checked", false);
	$(this).parents(".radio-wrap[data-name='"+name+"']").find("input").prop("checked", true);

});
// End


// This is for Popup Modals 
// Begin
$(".modals-trigger").on("click", function(e){
	e.preventDefault();
	var link = $(this).attr("data-target");

	$(link).addClass("active");
});

$(".m-modals .background-wrap").on("click", function(){
	$(this).parents(".m-modals").removeClass("active");
});
// End


// This Chosen JS Config 
// Begin
 $(".chosen-select").chosen({
 	no_results_text: "Sorry, nothing found!"
 }); 
// End

// This UI Datepicker 
// Begin
 $(".m-datepicker").datepicker({
 	format: 'dd/mm/yyyy',
 	changeMonth:true,
 	changeYear:true,
 	minDate: 0
 }); 
// End

});
