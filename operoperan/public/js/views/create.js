functions.push(function(){
	// This is Choosing player for add and delete player in user team 
	// Begin
	$(".add-player").on("click", function(e){
		e.preventDefault();

		var name = $(this).parents(".player-wrap").attr("data-name");
		var playerCard = $(this).parents(".player-wrap").html();
		var playerInput = $(".create-team").find("input[name=playerInput\\[\\]]");

		$(this).parents(".player-wrap").remove();
		$(".result-player").append("<div class='player-wrap' data-name='"+name+"'>"+playerCard+"</div>");
		playerInput.val(playerInput.val() + ","+name);

		$(".remove-player").on("click", function(e){
			e.preventDefault();

			var input = $(".create-team #inputPlayer").val();
			input = input.split(",");
			var playerDelete = $(this).parents(".player-wrap").attr("data-name");

			input = jQuery.grep(input, function(value) {
			  return value != playerDelete;
			});

			$(".create-team #inputPlayer").val(input);

			$(this).parents(".player-wrap").remove();
			// input = $.grep(input, function(value) {
			//   return value != playerDelete;
			// });
			// alert("This is "+input);
		});
	});

	$('div.foot a').click(function (e) {
		var email = $('input[name="email"]').val();
		var token = $('input[name="_token"]').val();
		var playerID = $('input[name="playerID"]').val();

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'operoperan-sendInvitation',
			data: {'email': email, '_token': token, 'playerID': playerID},
			success: function (res) {
				alert(res.msg);
			}
		});
	});

	//search players
	$('.m-searchInput input[type="text"]').keyup(function (e) {
		var key = $(this).val().toLowerCase();
		$('.player-wrap .player-visit .bio p').each(function () {
			var s = $(this).text().toLowerCase();
			$(this).closest('.player-wrap')[s.indexOf(key) !== -1 ? 'show' : 'hide']();
		});
	});
});