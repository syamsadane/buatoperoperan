functions.push(function(){
	$(".sign-wrap .tabs-wrap a").on("click", function(e){
		e.preventDefault();
		var href = $(this).attr("href");

		$(".sign-wrap .tabs-wrap a").removeClass("active");
		$(this).addClass("active");

		$(".form-wrap").addClass("hideit");
		$(href).removeClass("hideit");
	});

	$("#forgotButton").on("click", function(){
		$(this).parents(".form-wrap").addClass("hideit");

		$("#forgotpass").removeClass("hideit");
	});

	$(".image-browse input").on("change", function(){
		var file = $(this).files[0].name;
		console.log(file);
		$(this).parents(".image-browse").find("p").text(file);
	});
});