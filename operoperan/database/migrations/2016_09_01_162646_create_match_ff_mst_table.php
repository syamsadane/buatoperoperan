<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchFfMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MATCH_FF_MST', function (Blueprint $table) {
            $table->increments('MATCH_FF_MST_ID');
            $table->string('MATCH_FF_DATE');
            $table->string('MATCH_FF_TIME');
            $table->string('MATCH_FF_LOC');
            $table->string('MATCH_FF_TYPE');
            $table->string('MATCH_FF_FEE_TYPE');
            $table->integer('MATCH_FF_CREATED_BY')->unsigned();
            $table->boolean('MATCH_FF_FLAG')->default(false);
            $table->timestamps();

            $table->foreign('MATCH_FF_CREATED_BY')->references('PLYR_ID')->on('PLYR_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MATCH_FF_MST');
    }
}
