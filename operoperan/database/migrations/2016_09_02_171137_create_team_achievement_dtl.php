<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamAchievementDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TEAM_ACHIEVEMENT_DTL', function (Blueprint $table) {
            $table->increments('TEAM_ACHIEVEMENT_DTL_ID');
            $table->integer('TEAM_ID')->unsigned();
            $table->integer('ACHIEVMENT_ID')->unsigned();
            $table->timestamps();

            $table->foreign('TEAM_ID')->references('TEAM_MST_ID')->on('TEAM_MST');
            $table->foreign('ACHIEVMENT_ID')->references('ACHIEVEMENT_MST_ID')->on('ACHIEVEMENT_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TEAM_ACHIEVEMENT_DTL');
    }
}
