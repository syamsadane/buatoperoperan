<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievmentMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ACHIEVEMENT_MST', function (Blueprint $table) {
            $table->increments('ACHIEVEMENT_MST_ID');
            $table->string('ACHIEVEMENT_NAME');
            $table->string('ACHIEVEMENT_DESC');
            $table->string('ACHIEVEMENT_LOGO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ACHIEVEMENT_MST');
    }
}
