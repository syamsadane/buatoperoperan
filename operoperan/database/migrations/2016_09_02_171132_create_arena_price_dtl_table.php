<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArenaPriceDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ARENA_PRICE_DTL', function (Blueprint $table) {
            $table->increments('ARENA_PRICE_DTL_ID');
            $table->integer('ARENA_DTL_ID')->unsigned();
            $table->string('ARENA_HARI');
            $table->string('ARENA_JAM');
            $table->integer('ARENA_HARGA')->unsigned();
            $table->timestamps();

            $table->foreign('ARENA_DTL_ID')->references('ARENA_DTL_ID')->on('ARENA_DTL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('arena_price_dtl');
    }
}
