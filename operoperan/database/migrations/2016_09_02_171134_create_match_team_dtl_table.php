<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTeamDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MATCH_TEAM_DTL', function (Blueprint $table) {
            $table->increments('MATCH_TEAM_DTL_ID');
            $table->integer('MATCH_ID')->unsigned();
            $table->integer('TEAM_ID')->unsigned();
            $table->integer('SCORE')->unsigned();
            $table->timestamps();

            $table->foreign('MATCH_ID')->references('MATCH_MST_ID')->on('MATCH_MST');
            $table->foreign('TEAM_ID')->references('TEAM_MST_ID')->on('TEAM_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MATCH_TEAM_DTL');
    }
}
