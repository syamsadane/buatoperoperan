<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamPlyrDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TEAM_PLYR_DTL', function (Blueprint $table) {
            $table->increments('TEAM_PLYR_DTL_ID');
            $table->integer('TEAM_ID')->unsigned();
            $table->integer('PLYR_ID')->unsigned();
            $table->timestamps();

            $table->foreign('TEAM_ID')->references('TEAM_MST_ID')->on('TEAM_MST');
            $table->foreign('PLYR_ID')->references('PLYR_ID')->on('PLYR_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TEAM_PLYR_DTL');
    }
}
