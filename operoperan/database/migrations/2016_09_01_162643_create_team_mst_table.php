<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TEAM_MST', function (Blueprint $table) {
            $table->increments('TEAM_MST_ID');
            $table->string('TEAM_NAME')->index();
            $table->string('TEAM_LOGO');
            $table->string('TEAM_GENDER');
            $table->string('TEAM_DESC');
            $table->string('TEAM_CITY');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TEAM_MST');
    }
}
