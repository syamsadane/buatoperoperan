<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IMAGE_MST', function (Blueprint $table) {
            $table->increments('IMAGE_MST_ID');
            $table->string('IMAGE_NAME');
            $table->string('IMAGE_PATH');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('IMAGE_MST');
    }
}
