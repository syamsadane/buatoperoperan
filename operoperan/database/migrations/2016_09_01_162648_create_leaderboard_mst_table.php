<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaderboardMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LEADERBOARD_MST', function (Blueprint $table) {
            $table->increments('LEADERBOARD_ID');
            $table->integer('TEAM_ID')->unsigned();
            $table->integer('LEADERBOARD_PLAY');
            $table->integer('LEADERBOARD_WIN');
            $table->integer('LEADERBOARD_DRAW');
            $table->integer('LEADERBOARD_LOSE');
            $table->integer('LEADERBOARD_GF');
            $table->integer('LEADERBOARD_GA');
            $table->integer('LEADERBOARD_GD');
            $table->integer('LEADERBOARD_PTS');
            $table->timestamps();

            $table->foreign('TEAM_ID')->references('TEAM_MST_ID')->on('TEAM_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LEADERBOARD_MST');
    }
}
