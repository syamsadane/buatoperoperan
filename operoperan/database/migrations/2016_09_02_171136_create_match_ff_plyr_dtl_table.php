<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchFfPlyrDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MATCH_FF_PLYR_DTL', function (Blueprint $table) {
            $table->increments('MATCH_FF_PLYR_DTL_ID');
            $table->integer('MATCH_FF_ID')->unsigned();
            $table->integer('PLYR_ID')->unsigned();
            $table->timestamps();

            $table->foreign('MATCH_FF_ID')->references('MATCH_FF_MST_ID')->on('MATCH_FF_MST');
            $table->foreign('PLYR_ID')->references('PLYR_ID')->on('PLYR_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MATCH_FF_PLYR_DTL');
    }
}
