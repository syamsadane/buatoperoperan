<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArenaMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ARENA_MST', function (Blueprint $table) {
            $table->increments('ARENA_ID');
            $table->string('ARENA_NAME')->index();
            $table->string('ARENA_CITY');
            $table->string('ARENA_PHONE');
            $table->string('ARENA_DESC');
            $table->string('ARENA_FACILITY');
            $table->string('ARENA_ALTITUDE');
            $table->string('ARENA_LONGITUDE');
            $table->timestamps('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ARENA_MST');
    }
}
