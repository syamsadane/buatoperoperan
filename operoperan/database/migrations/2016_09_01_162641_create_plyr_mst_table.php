<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlyrMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PLYR_MST', function (Blueprint $table) {
            $table->increments('PLYR_ID');
            $table->string('PLYR_FIRST_NAME')->index();
            $table->string('PLYR_LAST_NAME');
            $table->string('PLYR_GENDER');
            $table->string('PLYR_DOB');
            $table->string('PLYR_PHONE');
            $table->string('PLYR_CITY', 50);
            $table->integer('PLYR_FAVOURITE_NO')->unsigned();
            $table->string('PLYR_EMAIL')->unique();
            $table->string('PLYR_PASSWORD');
            $table->integer('PLYR_HEIGHT')->unsigned();
            $table->integer('PLYR_WEIGHT')->unsigned();
            $table->integer('PLYR_AGE')->unsigned();
            $table->string('PLYR_BODY_BUILD');
            $table->string('PLYR_PROFILE_IMG', 255);
            $table->boolean('PLYR_FLAG_TEAM')->default(false);
            $table->boolean('PLYR_FLAG_PRM')->default(false);
            $table->boolean('PLYR_FLAG_ACTIVE')->default(false);
            $table->boolean('PLYR_FLAG_INVITED')->default(false);
            $table->integer('PLYR_NUMBER');
            $table->string('PLYR_ROLE');
            $table->boolean('PLYR_CONFIRMED')->default(false);
            $table->string('PLYR_CONFIRMATION_CODE')->nullable();
            $table->boolean('IS_ADMIN')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PLYR_MST');
    }
}
