<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamImageDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TEAM_IMAGE_DTL', function (Blueprint $table) {
            $table->increments('TEAM_IMAGE_DTL_ID');
            $table->integer('TEAM_ID')->unsigned();
            $table->integer('IMAGE_ID')->unsigned();
            $table->timestamps();

            $table->foreign('TEAM_ID')->references('TEAM_MST_ID')->on('TEAM_MST');
            $table->foreign('IMAGE_ID')->references('IMAGE_MST_ID')->on('IMAGE_MST');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TEAM_IMAGE_DTL');
    }
}
