<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MATCH_MST', function (Blueprint $table) {
            $table->increments('MATCH_MST_ID');
            $table->string('MATCH_DATE', 50);
            $table->string('MATCH_TIME');
            $table->integer('MATCH_LOC')->unsigned();
            $table->string('MATCH_TYPE');
            $table->string('MATCH_FEE_TYPE');
            $table->integer('MATCH_CREATED_BY')->unsigned();
            $table->boolean('MATCH_FLAG')->default(false);
            $table->timestamps();

            $table->foreign('MATCH_LOC')->references('ARENA_ID')->on('ARENA_MST');
            $table->foreign('MATCH_CREATED_BY')->references('PLYR_ID')->on('PLYR_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MATCH_MST');
    }
}
