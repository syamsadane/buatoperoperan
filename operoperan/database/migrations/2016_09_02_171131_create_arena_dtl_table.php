<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArenaDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ARENA_DTL', function (Blueprint $table) {
            $table->increments('ARENA_DTL_ID');
            $table->integer('ARENA_ID')->unsigned();
            $table->string('ARENA_TYPE');
            $table->string('ARENA_GRASS_TYPE');
            $table->integer('ARENA_JUMLAH');
            $table->timestamps();

            $table->foreign('ARENA_ID')->references('ARENA_ID')->on('ARENA_MST');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ARENA_DTL');
    }
}
