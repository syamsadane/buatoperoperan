<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Player::class, function (Faker\Generator $faker) {
    return [
        'PLYR_FIRST_NAME' => $faker->firstName,
        'PLYR_LAST_NAME' => $faker->lastName,
        'PLYR_GENDER' => 'M',
        'PLYR_DOB' => '07/02/1993',
        'PLYR_PHONE' => '0818661037',
        'PLYR_CITY' => 'Jakarta',
        'PLYR_FAVOURITE_NO' => 7,
        'PLYR_EMAIL' => $faker->safeEmail,
        'PLYR_PASSWORD' => bcrypt('asdasd'),
        'PLYR_PROFILE_IMG' => 'img_player_201609241474684490596.jpg',
        'remember_token' => str_random(10),
    ];
});